package kashyap.chandan.rtrapp.retrofit;

import javax.inject.Singleton;

import dagger.Component;
import kashyap.chandan.rtrapp.ChangePin;
import kashyap.chandan.rtrapp.ForgotPassword;
import kashyap.chandan.rtrapp.LoginScreen;
import kashyap.chandan.rtrapp.OtpVerification;
import kashyap.chandan.rtrapp.RDWDashboard;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.fragments.DailySheetFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseTypeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.HomeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.InvestmentFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ProfitLossFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.StatementFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.WithdrawalFragment;
import kashyap.chandan.rtrapp.SetPassword;

@Singleton
@Component(modules = ApiClientModule.class)
public interface ApiClientComponent {
    public void inject(MainActivity mainActivity);
    public void inject(ChangePin changePin);
    public void inject(ForgotPassword forgotPassword);
    public void inject(LoginScreen loginScreen);
    public void inject(OtpVerification otpVerification);
    public void inject(RDWDashboard rdwDashboard);
    public void inject(SetPassword setPassword);


    public void inject(DailySheetFragment dailySheetFragment);
    public void inject(ExpenseFragment expenseFragment);
    public void inject(ExpenseTypeFragment expenseTypeFragment);
    public void inject(HomeFragment homeFragment);

    public void inject(InvestmentFragment investmentFragment);
    public void inject(ProfitLossFragment profitLossFragment);
    public void inject(StatementFragment statementFragment);
    public void inject(WithdrawalFragment withdrawalFragment);
}
