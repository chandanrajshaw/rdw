package kashyap.chandan.rtrapp.retrofit;


import kashyap.chandan.rtrapp.response.rtr.AddExpenseResponse;
import kashyap.chandan.rtrapp.response.rtr.AddExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.AddInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.AddProfitLossResponse;
import kashyap.chandan.rtrapp.response.rtr.AddWithdrawalResponse;
import kashyap.chandan.rtrapp.response.ChangePasswordResponse;
import kashyap.chandan.rtrapp.response.rtr.DailySheetListResponse;
import kashyap.chandan.rtrapp.response.rtr.EditExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.EditInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.EditWithdrawalResponse;
import kashyap.chandan.rtrapp.response.rtr.ExpenseListResponse;
import kashyap.chandan.rtrapp.response.rtr.ExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.InvestmentListResponse;
import kashyap.chandan.rtrapp.response.rtr.LatestInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.ProfitLossListResponse;
import kashyap.chandan.rtrapp.response.rtr.RTRDashboardResponse;
import kashyap.chandan.rtrapp.response.rtr.RTRStatementResponse;
import kashyap.chandan.rtrapp.response.rtr.ViewInvestmentAmountResponse;
import kashyap.chandan.rtrapp.response.rtr.ViewInvestmentDetailResponse;
import kashyap.chandan.rtrapp.response.rtr.WithdrawalListRsponse;
import kashyap.chandan.rtrapp.response.loginResponse.ValidateOtpResponse;
import kashyap.chandan.rtrapp.response.loginResponse.LoginResponse;
import kashyap.chandan.rtrapp.response.loginResponse.ResetPasswordResponse;
import kashyap.chandan.rtrapp.response.loginResponse.SendOTPResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRExpenseResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRProfitLossResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRWithdrawalResponse;
import kashyap.chandan.rtrapp.response.str.EditSTRExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.str.EditSTRInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.EditSTRWithdrawalResponse;
import kashyap.chandan.rtrapp.response.str.STRAddInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.STRDailySheetListResponse;
import kashyap.chandan.rtrapp.response.str.STRDashboardResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseListResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.str.STRInvestmentListResponse;
import kashyap.chandan.rtrapp.response.str.STRLatestInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.STRProfitLossListResponse;
import kashyap.chandan.rtrapp.response.str.STRStatementResponse;
import kashyap.chandan.rtrapp.response.str.STRWithdrawalListRsponse;
import kashyap.chandan.rtrapp.response.str.ViewSTRInvestmentAmountResponse;
import kashyap.chandan.rtrapp.response.str.ViewSTRInvestmentDetailResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIInterface {
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Loginservice/login")Call<LoginResponse>login(@Field("phone") String phone,@Field("password") String password);

//
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Loginservice/sendotp")
    Call<SendOTPResponse> forgetPassword(@Field("email") String email);
////
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Loginservice/forpass")
    Call<ValidateOtpResponse> validateOtp(@Field("email") String email,
                                          @Field("otp") String otp);
////
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Loginservice/changepassotp")
    Call<ResetPasswordResponse> resetPassword(@Field("email") String email,
                                              @Field("newpass") String newpass);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Loginservice/changepass")
    Call<ChangePasswordResponse> changePassword(@Field("email") String email,
                                                @Field("oldpass") String oldpass,
                                                @Field("newpass") String newpass);
/*--------------------------------------RTR-----------------------------*/
    @Headers("X-API-KEY:rdw@123")
    @GET("Expensetypeservices/expensetype")
    Call<ExpenseTypeResponse> getExpenseTypeList();
//
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Expensesservices/add_expenses")
    Call<AddExpenseResponse> addExpense(@Field("expensetype_id") String expensetype_id,
                                        @Field("expen_date") String expen_date,
                                        @Field("amount") String amount,
                                        @Field("description") String description,
                                        @Field("status") String status);
    @Headers("X-API-KEY:rdw@123")
    @GET("Expensesservices/expenses")
    Call<ExpenseListResponse> getExpenseList();


    @Headers("X-API-KEY:rdw@123")
    @GET("Rtrdashservice/dashboard")
    Call<RTRDashboardResponse> getRTRResponse();



    @Headers("X-API-KEY:rdw@123")
    @GET("Investmentservices/investmentdatewise")
    Call<LatestInvestmentResponse> getLatestInvestment();

    @Headers("X-API-KEY:rdw@123")
    @GET("Withdrawalservices/withdrawal")
    Call<WithdrawalListRsponse> getWithdrawals();

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Withdrawalservices/add_withdrawal")
    Call<AddWithdrawalResponse>addWithdrawal(@Field("requested_date") String requested_date,
                                             @Field("reflected_date") String reflected_date,
                                             @Field("amount") String amount);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Withdrawalservices/edit_withdrawal")
    Call<EditWithdrawalResponse>editWithdrawal(@Field("withdrawal_id") String withdrawal_id,
                                               @Field("requested_date") String requested_date,
                                               @Field("reflected_date") String reflected_date,
                                               @Field("amount") String amount);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Investmentservices/add_investment")
    Call<AddInvestmentResponse>addInvestment(@Field("invest_date") String invest_date,
                                             @Field("amount") String amount);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Expensetypeservices/add_expensetype")
    Call<AddExpenseTypeResponse>addExpenseType(@Field("expensetype") String expensetype,
                                               @Field("status") String status);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Expensetypeservices/edit_expensetype")
    Call<EditExpenseTypeResponse>editExpenseType(@Field("expensetype_id") String expensetype_id,
                                                 @Field("expensetype") String expensetype,
                                                 @Field("status") String status);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Investmentservices/edit_investment")
    Call<EditInvestmentResponse>editInvestment(@Field("investment_id") String investment_id,
                                               @Field("invest_date") String invest_date,
                                               @Field("amount") String amount,
                                               @Field("edit_reason") String edit_reason);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Investmentservices/view_investment")
    Call<ViewInvestmentDetailResponse>ViewInvestment(@Field("investment_id") String investment_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Investmentservices/amount_investment")
    Call<ViewInvestmentAmountResponse>getInvestamount(@Field("investment_id") String investment_id);

//
@Headers("X-API-KEY:rdw@123")
@GET("dailysheetservices/dailysheet")
Call<DailySheetListResponse> getDailySheet();


    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Profitlossservices/add_profitloss")
    Call<AddProfitLossResponse>addProfitLoss(@Field("daily_date") String daily_date,
                                             @Field("profit_beforecommission") String profit_beforecommission,
                                             @Field("loss_beforecommission") String loss_beforecommission,
                                             @Field("profit_aftercommission") String profit_aftercommission,
                                             @Field("loss_aftercommission") String loss_aftercommission
    );

    @Headers("X-API-KEY:rdw@123")
    @GET("Investmentservices/investment")
    Call<InvestmentListResponse> getInvestments();

    @Headers("X-API-KEY:rdw@123")
    @GET("Profitlossservices/profitloss")
    Call<ProfitLossListResponse> getProfitLoss();

    @Headers("X-API-KEY:rdw@123")
    @GET("Dailysheetservices/dailysheetexcel")
    Call<RTRStatementResponse> getstatement();


    @Headers("X-API-KEY:rdw@123")
    @GET("dailysheetservices/statementdailysheet")
    Call<RTRStatementListResponse>getRTRStatement();

    /*---------------------------------------STR---------------------------*/
    @Headers("X-API-KEY:rdw@123")
    @GET("Strinvestmentservices/strinvestment")
    Call<STRInvestmentListResponse> getSTRInvestments();
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strinvestmentservices/add_strinvestment")
    Call<STRAddInvestmentResponse>addSTRInvestment(@Field("invest_date") String invest_date,
                                                   @Field("amount") String amount);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strinvestmentservices/edit_strinvestment")
    Call<EditSTRInvestmentResponse>editSTRInvestment(@Field("strinvestment_id") String strinvestment_id,
                                                     @Field("invest_date") String invest_date,
                                                     @Field("amount") String amount,
                                                     @Field("edit_reason") String edit_reason);

    @Headers("X-API-KEY:rdw@123")
    @GET("Strexpensetypeservices/strexpensetype")
    Call<STRExpenseTypeResponse> getSTRExpenseTypeList();

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strexpensetypeservices/add_strexpensetype")
    Call<AddSTRExpenseTypeResponse>addSTRExpenseType(@Field("strexpensetype") String expensetype,
                                                     @Field("status") String status);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strexpensetypeservices/edit_strexpensetype")
    Call<EditSTRExpenseTypeResponse>editSTRExpenseType(@Field("strexpensetype_id") String strexpensetype_id,
                                                       @Field("strexpensetype") String strexpensetype,
                                                       @Field("status") String status);

    @Headers("X-API-KEY:rdw@123")
    @GET("Strdashservice/dashboard")
    Call<STRDashboardResponse> getSTRResponse();

    @Headers("X-API-KEY:rdw@123")
    @GET("Strwithdrawalservices/strwithdrawal")
    Call<STRWithdrawalListRsponse> getSTRWithdrawals();

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strwithdrawalservices/add_strwithdrawal")
    Call<AddSTRWithdrawalResponse>addSTRWithdrawal(@Field("requested_date") String requested_date,
                                                   @Field("reflected_date") String reflected_date,
                                                   @Field("amount") String amount);
    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strwithdrawalservices/edit_strwithdrawal")
    Call<EditSTRWithdrawalResponse>editSTRWithdrawal(@Field("strwithdrawal_id") String withdrawal_id,
                                                     @Field("requested_date") String requested_date,
                                                     @Field("reflected_date") String reflected_date,
                                                     @Field("amount") String amount);

    @Headers("X-API-KEY:rdw@123")
    @GET("strdailysheetservices/strdailysheet")
    Call<STRDailySheetListResponse> getSTRDailySheet();

    @Headers("X-API-KEY:rdw@123")
    @GET("Strinvestmentservices/strinvestmentdatewise")
    Call<STRLatestInvestmentResponse> getLatestSTRInvestment();


    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strexpensesservices/add_strexpenses")
    Call<AddSTRExpenseResponse> addSTRExpense(@Field("strexpensetype_id") String strexpensetype_id,
                                              @Field("expen_date") String expen_date,
                                              @Field("amount") String amount,
                                              @Field("description") String description,
                                              @Field("status") String status);
    @Headers("X-API-KEY:rdw@123")
    @GET("Strexpensesservices/strexpenses")
    Call<STRExpenseListResponse> getSTRExpenseList();


    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("strinvestmentservices/view_strinvestment")
    Call<ViewSTRInvestmentDetailResponse>ViewSTRInvestment(@Field("strinvestment_id") String investment_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("strinvestmentservices/amount_strinvestment")
    Call<ViewSTRInvestmentAmountResponse>getSTRInvestamount(@Field("strinvestment_id") String investment_id);

    @FormUrlEncoded
    @Headers("X-API-KEY:rdw@123")
    @POST("Strprofitlossservices/add_strprofitloss")
    Call<AddSTRProfitLossResponse>addSTRProfitLoss(@Field("daily_date") String daily_date,
                                                   @Field("profit_beforecommission") String profit_beforecommission,
                                                   @Field("loss_beforecommission") String loss_beforecommission,
                                                   @Field("profit_aftercommission") String profit_aftercommission,
                                                   @Field("loss_aftercommission") String loss_aftercommission);
    @Headers("X-API-KEY:rdw@123")
    @GET("Strprofitlossservices/strprofitloss")
    Call<STRProfitLossListResponse> getSTRProfitLoss();

    @Headers("X-API-KEY:rdw@123")
    @GET("Strdailysheetservices/strdailysheetexcel")
    Call<STRStatementResponse> getSTRstatement();

}
