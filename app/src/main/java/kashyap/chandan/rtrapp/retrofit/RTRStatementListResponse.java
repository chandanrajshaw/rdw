package kashyap.chandan.rtrapp.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class RTRStatementListResponse implements Serializable {

    @Expose
    @SerializedName("statement")
    private List<StatementBean> Statement;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public List<StatementBean> getStatement() {
        return Statement;
    }

    public void setStatement(List<StatementBean> Statement) {
        this.Statement = Statement;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class StatementBean implements Serializable {
        @Expose
        @SerializedName("transaction_datetime")
        private String TransactionDatetime;
        @Expose
        @SerializedName("status")
        private String Status;
        @Expose
        @SerializedName("transaction_status")
        private String TransactionStatus;
        @Expose
        @SerializedName("debit_amount")
        private String DebitAmount;
        @Expose
        @SerializedName("credit_amount")
        private String CreditAmount;
        @Expose
        @SerializedName("transaction_date")
        private String TransactionDate;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getTransactionDatetime() {
            return TransactionDatetime;
        }

        public void setTransactionDatetime(String TransactionDatetime) {
            this.TransactionDatetime = TransactionDatetime;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getTransactionStatus() {
            return TransactionStatus;
        }

        public void setTransactionStatus(String TransactionStatus) {
            this.TransactionStatus = TransactionStatus;
        }

        public String getDebitAmount() {
            return DebitAmount;
        }

        public void setDebitAmount(String DebitAmount) {
            this.DebitAmount = DebitAmount;
        }

        public String getCreditAmount() {
            return CreditAmount;
        }

        public void setCreditAmount(String CreditAmount) {
            this.CreditAmount = CreditAmount;
        }

        public String getTransactionDate() {
            return TransactionDate;
        }

        public void setTransactionDate(String TransactionDate) {
            this.TransactionDate = TransactionDate;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean implements Serializable {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
