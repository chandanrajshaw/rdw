package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.response.loginResponse.ResetPasswordResponse;
import kashyap.chandan.rtrapp.response.loginResponse.ValidateOtpResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SetPassword extends AppCompatActivity {
TextView btn_submit;
    ImageView iv_back;
    Intent intent;
    Dialog dialog;
    EditText et_new_pin,et_confirm_pin;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intent!=null)
                {
                    String email=intent.getStringExtra("email");
                    String newPin=et_new_pin.getText().toString();
                    String conPin=et_confirm_pin.getText().toString();
                    if (newPin.isEmpty()&&conPin.isEmpty())
                        Snackbar.make(findViewById(android.R.id.content),"Enter New Pin",Snackbar.LENGTH_SHORT).show();
                    else if (newPin.isEmpty()||newPin.length()!=6)
                        Snackbar.make(findViewById(android.R.id.content),"Enter Valid 6-digit Pin",Snackbar.LENGTH_SHORT).show();
                    else if (conPin.isEmpty()||conPin.length()!=6)
                        Snackbar.make(findViewById(android.R.id.content),"Re-Enter Valid 6-digit Pin",Snackbar.LENGTH_SHORT).show();
                    else if (!conPin.equalsIgnoreCase(conPin))
                        Snackbar.make(findViewById(android.R.id.content),"Pin Mis-match",Snackbar.LENGTH_SHORT).show();
                    else
                    { setBtn_submit(email,conPin); }
                }
                else
                { Snackbar.make(findViewById(android.R.id.content),"Some Error Occured",Snackbar.LENGTH_SHORT).show(); }

            }
        });
    }


    private void init() {
        intent=getIntent();
        et_confirm_pin=findViewById(R.id.et_new_pin);
        et_new_pin =findViewById(R.id.et_old_pin);
        btn_submit=findViewById(R.id.btn_submit);
        iv_back=findViewById(R.id.iv_back);
    }
    private void setBtn_submit(String email,String pin)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<ResetPasswordResponse> call=apiInterface.resetPassword(email,pin);
        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content),"Password Changed Successfully ",Snackbar.LENGTH_SHORT).show();
                    Intent intent=new Intent(SetPassword.this,LoginScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Snackbar.make(findViewById(android.R.id.content),""+status.getMessage(),Snackbar.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                dialog.dismiss();
                Snackbar.make(findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}