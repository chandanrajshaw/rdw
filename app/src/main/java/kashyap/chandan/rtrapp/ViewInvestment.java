package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.rtrapp.RTRModule.adapters.EditHistoryAdapter;
import kashyap.chandan.rtrapp.response.rtr.ViewInvestmentAmountResponse;
import kashyap.chandan.rtrapp.response.rtr.ViewInvestmentDetailResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ViewInvestment extends AppCompatActivity {
RecyclerView historyRecycler;
Intent intent;
String id;
    ImageView iv_back;
TextView tv_expense_amount,tv_date,tv_avail_amount;
Dialog dialog;
List<ViewInvestmentDetailResponse.DataBean>history=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_investment);
        init();
        historyRecycler.setLayoutManager(new LinearLayoutManager(ViewInvestment.this,LinearLayoutManager.VERTICAL,false));

   if (intent!=null)
   {
       tv_date.setText(intent.getStringExtra("date"));
       tv_expense_amount.setText(intent.getStringExtra("amount"));
       id=intent.getStringExtra("id");
   }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getInvestmentDetail();
        getInvestmentAmount();
    }

    private void init() {
        iv_back=findViewById(R.id.iv_back);
        dialog=new Dialog(ViewInvestment.this);
        historyRecycler=findViewById(R.id.historyRecycler);
        intent=getIntent();
        tv_expense_amount=findViewById(R.id.tv_expense_amount);
        tv_date=findViewById(R.id.tv_date);
        tv_avail_amount=findViewById(R.id.tv_avail_amount);
    }
    private void getInvestmentAmount()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ViewInvestmentAmountResponse>call=apiInterface.getInvestamount(id);
        call.enqueue(new Callback<ViewInvestmentAmountResponse>() {
            @Override
            public void onResponse(Call<ViewInvestmentAmountResponse> call, Response<ViewInvestmentAmountResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                   tv_avail_amount.setText(response.body().getData());
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(ViewInvestment.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ViewInvestmentAmountResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ViewInvestment.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getInvestmentDetail()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<ViewInvestmentDetailResponse>call=apiInterface.ViewInvestment(id);
        call.enqueue(new Callback<ViewInvestmentDetailResponse>() {
            @Override
            public void onResponse(Call<ViewInvestmentDetailResponse> call, Response<ViewInvestmentDetailResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    history=response.body().getData();
                    historyRecycler.setAdapter(new EditHistoryAdapter(ViewInvestment.this,history));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(ViewInvestment.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ViewInvestmentDetailResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(ViewInvestment.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}