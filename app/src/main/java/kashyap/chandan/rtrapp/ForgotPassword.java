package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.regex.Pattern;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.response.loginResponse.SendOTPResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgotPassword extends AppCompatActivity {
TextView btn_submit;
ImageView iv_back;
EditText et_username;
Dialog dialog;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username=et_username.getText().toString();
                if (!emailValidation(username))
                    Snackbar.make(findViewById(android.R.id.content),"Enter Valid Email",Snackbar.LENGTH_SHORT).show();
                else
                {
                  setBtn_submit(username);
                }

            }
        });
    }
    private void setBtn_submit(String username)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<SendOTPResponse>call=apiInterface.forgetPassword(username);
        call.enqueue(new Callback<SendOTPResponse>() {
            @Override
            public void onResponse(Call<SendOTPResponse> call, Response<SendOTPResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content),"Otp Successfully Sent to your Registered Email",Snackbar.LENGTH_SHORT).show();
                    Intent intent=new Intent(ForgotPassword.this,OtpVerification.class);
                    intent.putExtra("email",username);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Snackbar.make(findViewById(android.R.id.content),""+status.getMessage(),Snackbar.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<SendOTPResponse> call, Throwable t) {
dialog.dismiss();
                Snackbar.make(findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

            }
        });
    }
    public boolean emailValidation(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email.isEmpty()||email == null)
            return false;
        return pat.matcher(email).matches();
    }
    private void init() {
        dialog=new Dialog(ForgotPassword.this);
        btn_submit=findViewById(R.id.btn_submit);
        iv_back=findViewById(R.id.iv_back);
        et_username=findViewById(R.id.et_username);
    }
}