package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.response.ChangePasswordResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChangePin extends AppCompatActivity {
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
ImageView iv_back;
EditText et_old_pin,et_new_pin;
TextView btn_submit;
Dialog dialog;
SharedPreferenceData preferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePin();
            }
        });
    }
private void changePin()
{
    String oldpin=et_old_pin.getText().toString().trim();
    String newpin=et_new_pin.getText().toString().trim();
    if (oldpin.isEmpty()&&newpin.isEmpty())
        Snackbar.make(findViewById(android.R.id.content),"Enter Both Field",Snackbar.LENGTH_SHORT).show();
    else if (oldpin.isEmpty()||oldpin.length()!=6)
        Snackbar.make(findViewById(android.R.id.content),"Enter old 6 digit Pin",Snackbar.LENGTH_SHORT).show();
    else if (newpin.isEmpty()||newpin.length()!=6)
        Snackbar.make(findViewById(android.R.id.content),"Enter new 6 digit Pin",Snackbar.LENGTH_SHORT).show();
    else if (oldpin.equals(newpin))
        Snackbar.make(findViewById(android.R.id.content),"Please choose Different Pin",Snackbar.LENGTH_SHORT).show();
    else
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<ChangePasswordResponse>call=apiInterface.changePassword(preferenceData.getEmail(),oldpin,newpin);
        call.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(ChangePin.this, "Pin Changed Successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                          retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Snackbar.make(findViewById(android.R.id.content),""+status.getMessage(),Snackbar.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(ChangePin.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }




}
    private void init() {
        preferenceData=new SharedPreferenceData(ChangePin.this);
        dialog=new Dialog(ChangePin.this);
        iv_back=findViewById(R.id.iv_back);
        btn_submit=findViewById(R.id.btn_submit);
        et_new_pin=findViewById(R.id.et_new_pin);
        et_old_pin=findViewById(R.id.et_old_pin);
    }
}