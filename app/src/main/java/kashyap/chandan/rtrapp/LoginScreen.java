package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.response.loginResponse.LoginResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginScreen extends AppCompatActivity {
EditText etPassword,et_phone;
ImageView password_toggle;
TextView forget_password,btnLogin;
Dialog dialog;
SharedPreferenceData preferenceData;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        password_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowHidePass(view);
            }
        });
        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginScreen.this,ForgotPassword.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              setBtnLogin();
            }
        });
    }

    private void init() {
        preferenceData=new SharedPreferenceData(LoginScreen.this);
        et_phone=findViewById(R.id.et_phone);
        btnLogin=findViewById(R.id.btnLogin);
        etPassword=findViewById(R.id.etPassword);
        password_toggle=findViewById(R.id.password_toggle);
        forget_password=findViewById(R.id.forget_password);
        dialog=new Dialog(LoginScreen.this);
    }
    public void ShowHidePass(View view) {

        if(view.getId()==R.id.password_toggle){
            if(etPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_visibility_off_white_24dp);
                //Show Password
                etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_visibility_black_24dp);
                //Hide Password
                etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }
    private void setBtnLogin()
    {
        String etphone=et_phone.getText().toString();
        String etpassword=etPassword.getText().toString().trim();
        if (etphone.isEmpty()&&etpassword.isEmpty())
            Snackbar.make(findViewById(android.R.id.content),"Fill All Fields",Snackbar.LENGTH_SHORT).show();
        else if (etphone.isEmpty()||etphone.length()!=10)
            Snackbar.make(findViewById(android.R.id.content),"Enter Valid Mobile Number",Snackbar.LENGTH_SHORT).show();
        else if (etpassword.isEmpty()||etpassword.length()!=6)
            Snackbar.make(findViewById(android.R.id.content),"Enter 6 digit Pin",Snackbar.LENGTH_SHORT).show();
        else
        {
            dialog.setContentView(R.layout.loadingdialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
            APIInterface apiInterface= retrofit.create(APIInterface.class);
            Call<LoginResponse>call=apiInterface.login(etphone,etpassword)  ;
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.code()==200)
                    {
                        LoginResponse.DetaBean data=response.body().getDeta();
                        preferenceData.putSharedPreference(data.getId(),data.getUsername(),data.getPhone(),data.getEmail(),data.getRole(),data.getImage(),data.getDateTimeUser());
                        dialog.dismiss();
                        Intent intent=new Intent(LoginScreen.this,RDWDashboard.class);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        dialog.dismiss();
                        Converter<ResponseBody, ApiError> converter =
                               retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                        ApiError error;
                        try {
                            error = converter.convert(response.errorBody());
                            ApiError.StatusBean status=error.getStatus();
                            Snackbar.make(findViewById(android.R.id.content),""+status.getMessage(),Snackbar.LENGTH_SHORT).show();
                        } catch (IOException e) { e.printStackTrace(); }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    dialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

                }
            });
        }

    }
}