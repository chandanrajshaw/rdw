package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class ExpenseTypeResponse implements Serializable {


    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("expensetype")
        private String expensetype;
        @Expose
        @SerializedName("id")
        private String id;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getExpensetype() {
            return expensetype;
        }

        public void setExpensetype(String expensetype) {
            this.expensetype = expensetype;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class StatusBean implements Serializable{
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
