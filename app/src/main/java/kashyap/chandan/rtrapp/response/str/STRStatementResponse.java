package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class STRStatementResponse {


    @Expose
    @SerializedName("EXCEL_strDAILYSHEET")
    private String ExcelStrdailysheet;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public String getExcelStrdailysheet() {
        return ExcelStrdailysheet;
    }

    public void setExcelStrdailysheet(String ExcelStrdailysheet) {
        this.ExcelStrdailysheet = ExcelStrdailysheet;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
