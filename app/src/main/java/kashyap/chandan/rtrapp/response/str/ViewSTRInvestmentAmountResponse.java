package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ViewSTRInvestmentAmountResponse {


    @Expose
    @SerializedName("data")
    private String Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public String getData() {
        return Data;
    }

    public void setData(String Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
