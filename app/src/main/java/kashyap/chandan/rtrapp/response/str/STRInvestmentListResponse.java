package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class STRInvestmentListResponse implements Serializable {

    @Expose
    @SerializedName("totalinvest")
    private TotalinvestBean Totalinvest;
    @Expose
    @SerializedName("lastinvest")
    private LastinvestBean Lastinvest;
    @Expose
    @SerializedName("oldinvest")
    private int Oldinvest;
    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public TotalinvestBean getTotalinvest() {
        return Totalinvest;
    }

    public void setTotalinvest(TotalinvestBean Totalinvest) {
        this.Totalinvest = Totalinvest;
    }

    public LastinvestBean getLastinvest() {
        return Lastinvest;
    }

    public void setLastinvest(LastinvestBean Lastinvest) {
        this.Lastinvest = Lastinvest;
    }

    public int getOldinvest() {
        return Oldinvest;
    }

    public void setOldinvest(int Oldinvest) {
        this.Oldinvest = Oldinvest;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class TotalinvestBean implements Serializable {
        @Expose
        @SerializedName("sum")
        private String Sum;

        public String getSum() {
            return Sum;
        }

        public void setSum(String Sum) {
            this.Sum = Sum;
        }
    }

    public static class LastinvestBean implements Serializable {
        @Expose
        @SerializedName("amount")
        private String Amount;

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String Amount) {
            this.Amount = Amount;
        }
    }

    public static class DataBean implements Serializable{
        @Expose
        @SerializedName("edit_reason")
        private String EditReason;
        @Expose
        @SerializedName("amount")
        private String Amount;
        @Expose
        @SerializedName("invest_date")
        private String InvestDate;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getEditReason() {
            return EditReason;
        }

        public void setEditReason(String EditReason) {
            this.EditReason = EditReason;
        }

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String Amount) {
            this.Amount = Amount;
        }

        public String getInvestDate() {
            return InvestDate;
        }

        public void setInvestDate(String InvestDate) {
            this.InvestDate = InvestDate;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean implements Serializable{
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
