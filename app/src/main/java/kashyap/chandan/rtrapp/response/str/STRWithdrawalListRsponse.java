package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class STRWithdrawalListRsponse {


    @Expose
    @SerializedName("total_withdrawal")
    private TotalWithdrawalBean TotalWithdrawal;
    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public TotalWithdrawalBean getTotalWithdrawal() {
        return TotalWithdrawal;
    }

    public void setTotalWithdrawal(TotalWithdrawalBean TotalWithdrawal) {
        this.TotalWithdrawal = TotalWithdrawal;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class TotalWithdrawalBean {
        @Expose
        @SerializedName("sum")
        private String Sum;

        public String getSum() {
            return Sum;
        }

        public void setSum(String Sum) {
            this.Sum = Sum;
        }
    }

    public static class DataBean {
        @Expose
        @SerializedName("money_after_strwithdrawal")
        private String MoneyAfterStrwithdrawal;
        @Expose
        @SerializedName("reflected_date")
        private String ReflectedDate;
        @Expose
        @SerializedName("amount")
        private String Amount;
        @Expose
        @SerializedName("requested_date")
        private String RequestedDate;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getMoneyAfterStrwithdrawal() {
            return MoneyAfterStrwithdrawal;
        }

        public void setMoneyAfterStrwithdrawal(String MoneyAfterStrwithdrawal) {
            this.MoneyAfterStrwithdrawal = MoneyAfterStrwithdrawal;
        }

        public String getReflectedDate() {
            return ReflectedDate;
        }

        public void setReflectedDate(String ReflectedDate) {
            this.ReflectedDate = ReflectedDate;
        }

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String Amount) {
            this.Amount = Amount;
        }

        public String getRequestedDate() {
            return RequestedDate;
        }

        public void setRequestedDate(String RequestedDate) {
            this.RequestedDate = RequestedDate;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
