package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ExpenseListResponse {


    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class DataBean {
        @Expose
        @SerializedName("expense")
        private String Expense;
        @Expose
        @SerializedName("initial_bal")
        private String InitialBal;
        @Expose
        @SerializedName("date")
        private String Date;

        public String getExpense() {
            return Expense;
        }

        public void setExpense(String Expense) {
            this.Expense = Expense;
        }

        public String getInitialBal() {
            return InitialBal;
        }

        public void setInitialBal(String InitialBal) {
            this.InitialBal = InitialBal;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
