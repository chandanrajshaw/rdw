package kashyap.chandan.rtrapp.response.loginResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class LoginResponse{
    @Expose
    @SerializedName("deta")
    private DetaBean Deta;
    @Expose
    @SerializedName("status")
    private StatusBean Status;
    public DetaBean getDeta() {
        return Deta;
    }
    public void setDeta(DetaBean Deta) {
        this.Deta = Deta;
    }
    public StatusBean getStatus() {
        return Status;
    }
    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }
    public static class DetaBean {
        @Expose
        @SerializedName("date_time_user")
        private String DateTimeUser;
        @Expose
        @SerializedName("status")
        private String Status;
        @Expose
        @SerializedName("role")
        private String Role;
        @Expose
        @SerializedName("otp")
        private String Otp;
        @Expose
        @SerializedName("image")
        private String Image;
        @Expose
        @SerializedName("u_id")
        private String UId;
        @Expose
        @SerializedName("password")
        private String Password;
        @Expose
        @SerializedName("phone")
        private String Phone;
        @Expose
        @SerializedName("email")
        private String Email;
        @Expose
        @SerializedName("username")
        private String Username;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getDateTimeUser() {
            return DateTimeUser;
        }

        public void setDateTimeUser(String DateTimeUser) {
            this.DateTimeUser = DateTimeUser;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String Status) {
            this.Status = Status;
        }

        public String getRole() {
            return Role;
        }

        public void setRole(String Role) {
            this.Role = Role;
        }

        public String getOtp() {
            return Otp;
        }

        public void setOtp(String Otp) {
            this.Otp = Otp;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getUId() {
            return UId;
        }

        public void setUId(String UId) {
            this.UId = UId;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String Phone) {
            this.Phone = Phone;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getUsername() {
            return Username;
        }

        public void setUsername(String Username) {
            this.Username = Username;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }
    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
