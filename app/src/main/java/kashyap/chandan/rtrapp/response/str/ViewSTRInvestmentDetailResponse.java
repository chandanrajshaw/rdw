package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ViewSTRInvestmentDetailResponse {


    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class DataBean {
        @Expose
        @SerializedName("edit_investdate")
        private String EditInvestdate;
        @Expose
        @SerializedName("edit_datetime")
        private String EditDatetime;
        @Expose
        @SerializedName("edit_reason")
        private String EditReason;
        @Expose
        @SerializedName("edit_amount")
        private String EditAmount;
        @Expose
        @SerializedName("strinvestment_id")
        private String StrinvestmentId;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getEditInvestdate() {
            return EditInvestdate;
        }

        public void setEditInvestdate(String EditInvestdate) {
            this.EditInvestdate = EditInvestdate;
        }

        public String getEditDatetime() {
            return EditDatetime;
        }

        public void setEditDatetime(String EditDatetime) {
            this.EditDatetime = EditDatetime;
        }

        public String getEditReason() {
            return EditReason;
        }

        public void setEditReason(String EditReason) {
            this.EditReason = EditReason;
        }

        public String getEditAmount() {
            return EditAmount;
        }

        public void setEditAmount(String EditAmount) {
            this.EditAmount = EditAmount;
        }

        public String getStrinvestmentId() {
            return StrinvestmentId;
        }

        public void setStrinvestmentId(String StrinvestmentId) {
            this.StrinvestmentId = StrinvestmentId;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
