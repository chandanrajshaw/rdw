package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class WithdrawalListRsponse implements Serializable {

    @Expose
    @SerializedName("total_withdrawal")
    private TotalWithdrawalBean totalWithdrawal;
    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public TotalWithdrawalBean getTotalWithdrawal() {
        return totalWithdrawal;
    }

    public void setTotalWithdrawal(TotalWithdrawalBean totalWithdrawal) {
        this.totalWithdrawal = totalWithdrawal;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class TotalWithdrawalBean  implements Serializable{
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("money_after_withdrawal")
        private String moneyAfterWithdrawal;
        @Expose
        @SerializedName("reflected_date")
        private String reflectedDate;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("requested_date")
        private String requestedDate;
        @Expose
        @SerializedName("id")
        private String id;

        public String getMoneyAfterWithdrawal() {
            return moneyAfterWithdrawal;
        }

        public void setMoneyAfterWithdrawal(String moneyAfterWithdrawal) {
            this.moneyAfterWithdrawal = moneyAfterWithdrawal;
        }

        public String getReflectedDate() {
            return reflectedDate;
        }

        public void setReflectedDate(String reflectedDate) {
            this.reflectedDate = reflectedDate;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getRequestedDate() {
            return requestedDate;
        }

        public void setRequestedDate(String requestedDate) {
            this.requestedDate = requestedDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
