package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class RTRStatementResponse {


    @Expose
    @SerializedName("EXCEL_DAILYSHEET")
    private String ExcelDailysheet;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public String getExcelDailysheet() {
        return ExcelDailysheet;
    }

    public void setExcelDailysheet(String ExcelDailysheet) {
        this.ExcelDailysheet = ExcelDailysheet;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}
