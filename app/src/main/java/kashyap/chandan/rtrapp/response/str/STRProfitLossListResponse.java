package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class STRProfitLossListResponse {


    @Expose
    @SerializedName("total_loss")
    private TotalLossBean totalLoss;
    @Expose
    @SerializedName("total_profit")
    private TotalProfitBean totalProfit;
    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public TotalLossBean getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(TotalLossBean totalLoss) {
        this.totalLoss = totalLoss;
    }

    public TotalProfitBean getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(TotalProfitBean totalProfit) {
        this.totalProfit = totalProfit;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class TotalLossBean {
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class TotalProfitBean {
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class DataBean {
        @Expose
        @SerializedName("closing_balance")
        private String closingBalance;
        @Expose
        @SerializedName("opening_balance")
        private String openingBalance;
        @Expose
        @SerializedName("loss_beforecommission")
        private String lossBeforecommission;
        @Expose
        @SerializedName("profit_beforecommission")
        private String profitBeforecommission;
        @Expose
        @SerializedName("loss")
        private String loss;
        @Expose
        @SerializedName("profit")
        private String profit;
        @Expose
        @SerializedName("expense_tilldate")
        private String expenseTilldate;
        @Expose
        @SerializedName("daily_date")
        private String dailyDate;
        @Expose
        @SerializedName("id")
        private String id;

        public String getClosingBalance() {
            return closingBalance;
        }

        public void setClosingBalance(String closingBalance) {
            this.closingBalance = closingBalance;
        }

        public String getOpeningBalance() {
            return openingBalance;
        }

        public void setOpeningBalance(String openingBalance) {
            this.openingBalance = openingBalance;
        }

        public String getLossBeforecommission() {
            return lossBeforecommission;
        }

        public void setLossBeforecommission(String lossBeforecommission) {
            this.lossBeforecommission = lossBeforecommission;
        }

        public String getProfitBeforecommission() {
            return profitBeforecommission;
        }

        public void setProfitBeforecommission(String profitBeforecommission) {
            this.profitBeforecommission = profitBeforecommission;
        }

        public String getLoss() {
            return loss;
        }

        public void setLoss(String loss) {
            this.loss = loss;
        }

        public String getProfit() {
            return profit;
        }

        public void setProfit(String profit) {
            this.profit = profit;
        }

        public String getExpenseTilldate() {
            return expenseTilldate;
        }

        public void setExpenseTilldate(String expenseTilldate) {
            this.expenseTilldate = expenseTilldate;
        }

        public String getDailyDate() {
            return dailyDate;
        }

        public void setDailyDate(String dailyDate) {
            this.dailyDate = dailyDate;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
