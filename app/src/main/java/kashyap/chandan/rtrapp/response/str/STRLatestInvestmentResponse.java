package kashyap.chandan.rtrapp.response.str;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class STRLatestInvestmentResponse {


    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class DataBean {
        @Expose
        @SerializedName("total_invest")
        private String totalInvest;
        @Expose
        @SerializedName("invest_amount")
        private String investAmount;
        @Expose
        @SerializedName("date")
        private String date;

        public String getTotalInvest() {
            return totalInvest;
        }

        public void setTotalInvest(String totalInvest) {
            this.totalInvest = totalInvest;
        }

        public String getInvestAmount() {
            return investAmount;
        }

        public void setInvestAmount(String investAmount) {
            this.investAmount = investAmount;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
