package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class DailySheetListResponse implements Serializable {


    @Expose
    @SerializedName("data")
    private List<DataBean> Data;
    @Expose
    @SerializedName("status")
    private StatusBean Status;

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public StatusBean getStatus() {
        return Status;
    }

    public void setStatus(StatusBean Status) {
        this.Status = Status;
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("closing_balance")
        private String ClosingBalance;
        @Expose
        @SerializedName("opening_balance")
        private String OpeningBalance;
        @Expose
        @SerializedName("loss_beforecommission")
        private String LossBeforecommission;
        @Expose
        @SerializedName("profit_beforecommission")
        private String ProfitBeforecommission;
        @Expose
        @SerializedName("loss")
        private String Loss;
        @Expose
        @SerializedName("profit")
        private String Profit;
        @Expose
        @SerializedName("withdrawal_tilldate")
        private String WithdrawalTilldate;
        @Expose
        @SerializedName("expense_tilldate")
        private String ExpenseTilldate;
        @Expose
        @SerializedName("daily_date")
        private String DailyDate;
        @Expose
        @SerializedName("id")
        private String Id;

        public String getClosingBalance() {
            return ClosingBalance;
        }

        public void setClosingBalance(String ClosingBalance) {
            this.ClosingBalance = ClosingBalance;
        }

        public String getOpeningBalance() {
            return OpeningBalance;
        }

        public void setOpeningBalance(String OpeningBalance) {
            this.OpeningBalance = OpeningBalance;
        }

        public String getLossBeforecommission() {
            return LossBeforecommission;
        }

        public void setLossBeforecommission(String LossBeforecommission) {
            this.LossBeforecommission = LossBeforecommission;
        }

        public String getProfitBeforecommission() {
            return ProfitBeforecommission;
        }

        public void setProfitBeforecommission(String ProfitBeforecommission) {
            this.ProfitBeforecommission = ProfitBeforecommission;
        }

        public String getLoss() {
            return Loss;
        }

        public void setLoss(String Loss) {
            this.Loss = Loss;
        }

        public String getProfit() {
            return Profit;
        }

        public void setProfit(String Profit) {
            this.Profit = Profit;
        }

        public String getWithdrawalTilldate() {
            return WithdrawalTilldate;
        }

        public void setWithdrawalTilldate(String WithdrawalTilldate) {
            this.WithdrawalTilldate = WithdrawalTilldate;
        }

        public String getExpenseTilldate() {
            return ExpenseTilldate;
        }

        public void setExpenseTilldate(String ExpenseTilldate) {
            this.ExpenseTilldate = ExpenseTilldate;
        }

        public String getDailyDate() {
            return DailyDate;
        }

        public void setDailyDate(String DailyDate) {
            this.DailyDate = DailyDate;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String Message;
        @Expose
        @SerializedName("code")
        private int Code;

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public int getCode() {
            return Code;
        }

        public void setCode(int Code) {
            this.Code = Code;
        }
    }
}