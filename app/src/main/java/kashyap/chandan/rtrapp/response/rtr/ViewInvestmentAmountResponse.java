package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ViewInvestmentAmountResponse {

    @Expose
    @SerializedName("data")
    private String data;
    @Expose
    @SerializedName("status")
    private StatusEntity status;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public static class StatusEntity {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
