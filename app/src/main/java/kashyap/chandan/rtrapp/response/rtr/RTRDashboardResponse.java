package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class RTRDashboardResponse {


    @Expose
    @SerializedName("currentbalance")
    private int currentbalance;
    @Expose
    @SerializedName("TotalWithdrawal")
    private TotalwithdrawalBean totalwithdrawal;
    @Expose
    @SerializedName("total_loss")
    private TotalLossBean totalLoss;
    @Expose
    @SerializedName("total_profit")
    private TotalProfitBean totalProfit;
    @Expose
    @SerializedName("TotalExpense")
    private TotalexpenseBean totalexpense;
    @Expose
    @SerializedName("TotalInvestment")
    private TotalinvestmentBean totalinvestment;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public int getCurrentbalance() {
        return currentbalance;
    }

    public void setCurrentbalance(int currentbalance) {
        this.currentbalance = currentbalance;
    }

    public TotalwithdrawalBean getTotalwithdrawal() {
        return totalwithdrawal;
    }

    public void setTotalwithdrawal(TotalwithdrawalBean totalwithdrawal) {
        this.totalwithdrawal = totalwithdrawal;
    }

    public TotalLossBean getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(TotalLossBean totalLoss) {
        this.totalLoss = totalLoss;
    }

    public TotalProfitBean getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(TotalProfitBean totalProfit) {
        this.totalProfit = totalProfit;
    }

    public TotalexpenseBean getTotalexpense() {
        return totalexpense;
    }

    public void setTotalexpense(TotalexpenseBean totalexpense) {
        this.totalexpense = totalexpense;
    }

    public TotalinvestmentBean getTotalinvestment() {
        return totalinvestment;
    }

    public void setTotalinvestment(TotalinvestmentBean totalinvestment) {
        this.totalinvestment = totalinvestment;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class TotalwithdrawalBean {
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class TotalLossBean {
        @Expose
        @SerializedName("losssum")
        private String losssum;

        public String getLosssum() {
            return losssum;
        }

        public void setLosssum(String losssum) {
            this.losssum = losssum;
        }
    }

    public static class TotalProfitBean {
        @Expose
        @SerializedName("profitsum")
        private String profitsum;

        public String getProfitsum() {
            return profitsum;
        }

        public void setProfitsum(String profitsum) {
            this.profitsum = profitsum;
        }
    }

    public static class TotalexpenseBean {
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class TotalinvestmentBean {
        @Expose
        @SerializedName("sum")
        private String sum;

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }
    }

    public static class StatusBean {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
