package kashyap.chandan.rtrapp.response.rtr;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public  class ViewInvestmentDetailResponse implements Serializable {

    @Expose
    @SerializedName("data")
    private List<DataBean> data;
    @Expose
    @SerializedName("status")
    private StatusBean status;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public static class DataBean implements Serializable {
        @Expose
        @SerializedName("edit_investdate")
        private String editInvestdate;
        @Expose
        @SerializedName("edit_datetime")
        private String editDatetime;
        @Expose
        @SerializedName("edit_reason")
        private String editReason;
        @Expose
        @SerializedName("edit_amount")
        private String editAmount;
        @Expose
        @SerializedName("investment_id")
        private String investmentId;
        @Expose
        @SerializedName("id")
        private String id;

        public String getEditInvestdate() {
            return editInvestdate;
        }

        public void setEditInvestdate(String editInvestdate) {
            this.editInvestdate = editInvestdate;
        }

        public String getEditDatetime() {
            return editDatetime;
        }

        public void setEditDatetime(String editDatetime) {
            this.editDatetime = editDatetime;
        }

        public String getEditReason() {
            return editReason;
        }

        public void setEditReason(String editReason) {
            this.editReason = editReason;
        }

        public String getEditAmount() {
            return editAmount;
        }

        public void setEditAmount(String editAmount) {
            this.editAmount = editAmount;
        }

        public String getInvestmentId() {
            return investmentId;
        }

        public void setInvestmentId(String investmentId) {
            this.investmentId = investmentId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class StatusBean implements Serializable {
        @Expose
        @SerializedName("message")
        private String message;
        @Expose
        @SerializedName("code")
        private int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
