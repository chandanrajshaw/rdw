package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.response.loginResponse.ValidateOtpResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OtpVerification extends AppCompatActivity {
TextView btn_submit,btn_send;
Dialog dialog;
EditText et_otp;
ImageView iv_back;
Intent intent;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intent!=null)
                {
                    String email=intent.getStringExtra("email");
                    String otp=et_otp.getText().toString();
                    if (otp.isEmpty()||otp.length()!=6)
                        Snackbar.make(findViewById(android.R.id.content),"Enter Valid OTP",Snackbar.LENGTH_SHORT).show();
                    else
                    {
                     setBtn_submit(email,otp);
                    }
                }
                else
                {
                    Snackbar.make(findViewById(android.R.id.content),"Some Error Occured",Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void init() {
        iv_back=findViewById(R.id.iv_back);
        et_otp=findViewById(R.id.et_otp);
        dialog=new Dialog(OtpVerification.this);
        btn_submit=findViewById(R.id.btn_submit);
        btn_send=findViewById(R.id.btn_send);
        intent=getIntent();
    }
    private void setBtn_submit(String email,String otp)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<ValidateOtpResponse> call=apiInterface.validateOtp(email,otp);
        call.enqueue(new Callback<ValidateOtpResponse>() {
            @Override
            public void onResponse(Call<ValidateOtpResponse> call, Response<ValidateOtpResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content),"Otp Successfully Verified ",Snackbar.LENGTH_SHORT).show();
                    Intent intent=new Intent(OtpVerification.this,SetPassword.class);
                    intent.putExtra("email",email);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Snackbar.make(findViewById(android.R.id.content),""+status.getMessage(),Snackbar.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ValidateOtpResponse> call, Throwable t) {
                dialog.dismiss();
                Snackbar.make(findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}