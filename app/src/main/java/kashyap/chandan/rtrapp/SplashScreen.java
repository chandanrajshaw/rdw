package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreen extends AppCompatActivity {
SharedPreferenceData preferenceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        preferenceData=new SharedPreferenceData(SplashScreen.this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (preferenceData.getPhone()!=null)
                {
                    Intent intent=new Intent(SplashScreen.this,RDWDashboard.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent=new Intent(SplashScreen.this,LoginScreen.class);
                    startActivity(intent);
                    finish();
                }

            }
        },1000);
    }
}