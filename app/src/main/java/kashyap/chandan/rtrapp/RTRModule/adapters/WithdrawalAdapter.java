package kashyap.chandan.rtrapp.RTRModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.CustomItemClickListenerObject;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.WithdrawalListRsponse;

public class WithdrawalAdapter extends RecyclerView.Adapter<WithdrawalAdapter.MyViewHolder> {
    Context context;
    List<WithdrawalListRsponse.DataBean> dataBeans;
    CustomItemClickListenerObject listenerObject;
    public WithdrawalAdapter(Context context, List<WithdrawalListRsponse.DataBean> dataBeans, CustomItemClickListenerObject listenerObject) {
      this.context=context;
      this.dataBeans=dataBeans;
      this.listenerObject=listenerObject;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.withdrawal_request_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.tv_available_amount.setText("Available Amount :"+dataBeans.get(position).getMoneyAfterWithdrawal());
holder.tv_date.setText(dataBeans.get(position).getRequestedDate());
holder.requested_text.setText("Withdrawal request has been raised on "+dataBeans.get(position).getRequestedDate()+" of an amount of Rs. "+dataBeans.get(position).getAmount()+" will be credited on "+dataBeans.get(position).getReflectedDate());
holder.tv_amount.setText(dataBeans.get(position).getAmount());
    }
    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_date,tv_available_amount,requested_text,tv_amount;
        ImageView iv_edit;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_edit=itemView.findViewById(R.id.iv_edit);
            tv_amount=itemView.findViewById(R.id.tv_amount);
            requested_text=itemView.findViewById(R.id.requested_text);
            tv_available_amount=itemView.findViewById(R.id.tv_available_amount);
            tv_date=itemView.findViewById(R.id.tv_date);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listenerObject.onItemClickListener(view,dataBeans.get(getAdapterPosition()));
                }
            };
            iv_edit.setOnClickListener(clickListener);
        }
    }
}
