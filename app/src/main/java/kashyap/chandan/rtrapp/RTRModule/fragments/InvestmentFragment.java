package kashyap.chandan.rtrapp.RTRModule.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.adapters.InvestAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.EditInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.InvestmentListResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class InvestmentFragment extends Fragment {
    RecyclerView recyclerView;
    Context context;
    CircleImageView add;
    MainActivity dashboard;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    Calendar myCalendar,calendar;
    DatePickerDialog.OnDateSetListener dateDialog;
TextView tvDate;
    List<InvestmentListResponse.DataBean>dataBeans=new ArrayList<>();
Dialog editInvestmentDialog,addInvestmentDialog,dialog;
    public InvestmentFragment(Context context) {
        this.context = context;
        this.dashboard = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.investment_fragment,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        recyclerView=view.findViewById(R.id.recyclerView);
        add=view.findViewById(R.id.add);
        myCalendar=Calendar.getInstance();
        calendar=Calendar.getInstance();
        dialog=new Dialog(dashboard);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
addInvestmentDialog();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));


    }

    @Override
    public void onResume() {
        super.onResume();
        getInvestments();
    }

    private void editInvestmentDialog(String id, String amount)
    {
        editInvestmentDialog=new Dialog(dashboard);
        editInvestmentDialog.setContentView(R.layout.edit_investment_dialog);
        editInvestmentDialog.setCancelable(false);
        editInvestmentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close=editInvestmentDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editInvestmentDialog.dismiss();
            }
        });
       tvDate=editInvestmentDialog.findViewById(R.id.tv_date);
        EditText et_amount=editInvestmentDialog.findViewById(R.id.et_initial_amount);
        et_amount.setText(amount);

        EditText et_reason=editInvestmentDialog.findViewById(R.id.et_reason);
        TextView btnEdit=editInvestmentDialog.findViewById(R.id.btnEdit);
        tvDate.setText(updateLabel(myCalendar));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reason=et_reason.getText().toString().trim();
                String amt=et_amount.getText().toString().trim();
                String date=tvDate.getText().toString();
                double checkamt=Double.parseDouble(amt);
                if (reason.isEmpty()&&amt.isEmpty())
                    Toast.makeText(context, "Fill All the Fields", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty()||checkamt<=0.0)
                    Toast.makeText(context, "Enter Amount", Toast.LENGTH_SHORT).show();
                else if (reason.isEmpty())
                    Toast.makeText(context, "Enter Reason", Toast.LENGTH_SHORT).show();
               else
                {
                editInvestment(id,date,amt,reason);
                }

            }
        });
        editInvestmentDialog.show();

    }
    private void editInvestment(String id,String date,String amount,String reason)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<EditInvestmentResponse>call=apiInterface.editInvestment(id,date,amount,reason);
        call.enqueue(new Callback<EditInvestmentResponse>() {
            @Override
            public void onResponse(Call<EditInvestmentResponse> call, Response<EditInvestmentResponse> response) {
                if (response.code()==200)
                {
                 dialog.dismiss();
                    Toast.makeText(dashboard, "Investment Edited Successfully", Toast.LENGTH_SHORT).show();
                    editInvestmentDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<EditInvestmentResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void addInvestmentDialog()
    {
        addInvestmentDialog=new Dialog(dashboard);
        addInvestmentDialog.setContentView(R.layout.add_investment_dialog);
        addInvestmentDialog.setCancelable(false);
        addInvestmentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close=addInvestmentDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addInvestmentDialog.dismiss();
            }
        });
       TextView tvDate=addInvestmentDialog.findViewById(R.id.tv_date);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme,dateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvDate.setText(updateLabel(calendar));
            }
        };
        EditText et_amount=addInvestmentDialog.findViewById(R.id.et_initial_amount);
        TextView btnEdit=addInvestmentDialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date=tvDate.getText().toString();
                String amt=et_amount.getText().toString().trim();
                double checkamt=Double.parseDouble(amt);
                if (date.equalsIgnoreCase("Select Date")&&amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else if (date.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Date", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty()||checkamt<=0.0)
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else
                {
                    addInvestment(date,amt);
                }

            }
        });
        addInvestmentDialog.show();

    }
    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
    private void addInvestment(String date,String amount)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<AddInvestmentResponse>call=apiInterface.addInvestment(date,amount);
        call.enqueue(new Callback<AddInvestmentResponse>() {
            @Override
            public void onResponse(Call<AddInvestmentResponse> call, Response<AddInvestmentResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashboard, "Added Successfully", Toast.LENGTH_SHORT).show();
                    addInvestmentDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<AddInvestmentResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getInvestments()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=retrofit.create(APIInterface.class);
        Call<InvestmentListResponse>call=apiInterface.getInvestments();
        call.enqueue(new Callback<InvestmentListResponse>() {
            @Override
            public void onResponse(Call<InvestmentListResponse> call, Response<InvestmentListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                   dataBeans=response.body().getData();
                    recyclerView.setAdapter(new InvestAdapter(dashboard,dataBeans,new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String amount) {
                            editInvestmentDialog(id,amount);
                        }
                    }));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<InvestmentListResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
