package kashyap.chandan.rtrapp.RTRModule.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.ExpenseTypeResponse;

public class ExpenseTypeAdapter extends RecyclerView.Adapter<ExpenseTypeAdapter.MyViewHolder> {
    Context context;
    CustomItemClickListener listener;
    List<ExpenseTypeResponse.DataBean> expenseList;
    Dialog expenseDialog;
    private int mSelectedItem = -1;
    public ExpenseTypeAdapter(Context context, List<ExpenseTypeResponse.DataBean> expenseList, Dialog expenseDialog, CustomItemClickListener listener) {
   this.context=context;
   this.expenseDialog=expenseDialog;
   this.expenseList=expenseList;
   this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.item.setText(expenseList.get(position).getExpensetype());
        TextView ok= expenseDialog.findViewById(R.id.ok);
        TextView dialogheader=expenseDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Expense Type");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Expense Type", Toast.LENGTH_SHORT).show(); }
                else
                { expenseDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView item;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            item=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, expenseList.get(mSelectedItem).getId(),expenseList.get(mSelectedItem).getExpensetype());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
