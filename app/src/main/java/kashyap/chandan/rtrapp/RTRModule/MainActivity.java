package kashyap.chandan.rtrapp.RTRModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.ChangePin;
import kashyap.chandan.rtrapp.LoginScreen;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.fragments.DailySheetFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseTypeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.HomeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.InvestmentFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ProfitLossFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.StatementFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.WithdrawalFragment;
import kashyap.chandan.rtrapp.SharedPreferenceData;
import kashyap.chandan.rtrapp.response.rtr.EditWithdrawalResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.RTRStatementListResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    ImageView iv_menu;
    TextView tv_logout;
    float prev_closing=0;
    float open_bal=0;
    String trans;
    String amount;
    float closing_bal=0;
    Dialog dialog, transactionDialog;
    List<RTRStatementListResponse.StatementBean> statementBeans=new ArrayList<>();
    SharedPreferenceData preferenceData;
    List<StatementPojo> statementPojoList=new ArrayList<>();
    Calendar calendar;
    ApiClientComponent apiClientComponent;
    DatePickerDialog.OnDateSetListener startDateDialog,toDateDialog;
    RelativeLayout nav_statement_layout,nav_profit_loss_layout,nav_ecpense_type_layout,nav_change_pin_layout,nav_expense_layout,nav_withdraw_layout,nav_home_layout,nav_investment_layout,nav_daily_sheet_layout;
   @Inject
    Retrofit retrofit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        nav_statement_layout.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        nav_profit_loss_layout.setOnClickListener(this);
        nav_ecpense_type_layout.setOnClickListener(this);
        nav_change_pin_layout.setOnClickListener(this);
        nav_expense_layout.setOnClickListener(this);
        nav_withdraw_layout.setOnClickListener(this);
        nav_home_layout.setOnClickListener(this);
        nav_investment_layout.setOnClickListener(this);
        nav_daily_sheet_layout.setOnClickListener(this);
        HomeFragment homeFragment=new HomeFragment(MainActivity.this);
        loadFragment(homeFragment);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else
                {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }
    private Date parseDate(String date, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }
    private void init() {
        calendar=Calendar.getInstance();
        dialog=new Dialog(MainActivity.this);
        preferenceData=new SharedPreferenceData(MainActivity.this);
        tv_logout=findViewById(R.id.tv_logout);
        nav_profit_loss_layout=findViewById(R.id.nav_profit_loss_layout);
        nav_ecpense_type_layout=findViewById(R.id.nav_ecpense_type_layout);
        nav_change_pin_layout=findViewById(R.id.nav_change_pin_layout);
        nav_investment_layout=findViewById(R.id.nav_investment_layout);
        nav_home_layout=findViewById(R.id.nav_home_layout);
        nav_withdraw_layout=findViewById(R.id.nav_withdraw_layout);
        nav_expense_layout=findViewById(R.id.nav_expense_layout);
        nav_daily_sheet_layout=findViewById(R.id.nav_daily_sheet_layout);
        iv_menu=findViewById(R.id.iv_menu);
        drawerLayout=findViewById(R.id.drawerLayout);
        nav_statement_layout=findViewById(R.id.nav_statement_layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.nav_home_layout:
                HomeFragment  homeFragment=new HomeFragment(MainActivity.this);
                loadFragment(homeFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_expense_layout:
              ExpenseFragment expenseFragment=new ExpenseFragment(MainActivity.this);
                loadFragment(expenseFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_withdraw_layout:
                WithdrawalFragment  withdrawalFragment=new WithdrawalFragment(MainActivity.this);
                loadFragment(withdrawalFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_investment_layout:
                InvestmentFragment investmentFragment=new InvestmentFragment(MainActivity.this);
                loadFragment(investmentFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_daily_sheet_layout:
                DailySheetFragment dailySheetFragment=new DailySheetFragment(MainActivity.this);
                loadFragment(dailySheetFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_change_pin_layout:
                Intent intent=new Intent(MainActivity.this, ChangePin.class);
                startActivity(intent);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case  R.id.tv_logout:
                Intent intent1=new Intent(MainActivity.this, LoginScreen.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                preferenceData.sessionEnd();
                startActivity(intent1);
                finish();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_ecpense_type_layout:
                ExpenseTypeFragment expenseTypeFragment=new ExpenseTypeFragment(MainActivity.this);
                loadFragment(expenseTypeFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_profit_loss_layout:
                ProfitLossFragment profitLossFragment=new ProfitLossFragment(MainActivity.this);
                loadFragment(profitLossFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_statement_layout:
//              getTransactionDialog();
                getInvestmentList();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }
   private void loadFragment(Fragment fragment)
    {
        if (fragment!=null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.main_frame, fragment);
            ft.commit();
        }
    }
//    private void getTransactionDialog()
//    {
//        transactionDialog =new Dialog(MainActivity.this);
//        transactionDialog.setContentView(R.layout.statement_dialog);
//        transactionDialog.setCancelable(false);
//        transactionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        TextView header= transactionDialog.findViewById(R.id.header);
//        header.setText("Statement");
//        ImageView close= transactionDialog.findViewById(R.id.close);
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                transactionDialog.dismiss();
//            }
//        });
//        TextView tv_start_date= transactionDialog.findViewById(R.id.tv_start_date);
//        TextView tv_to_date= transactionDialog.findViewById(R.id.tv_to_date);
//        tv_to_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new DatePickerDialog(MainActivity.this,R.style.TimePickerTheme, toDateDialog, calendar
//                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//        tv_start_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new DatePickerDialog(MainActivity.this,R.style.TimePickerTheme, startDateDialog, calendar
//                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//        startDateDialog =new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
//                calendar.set(Calendar.YEAR, year);
//                calendar.set(Calendar.MONTH, monthOfYear);
//                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                tv_start_date.setText(updateLabel(calendar));
//            }
//        };
//        toDateDialog =new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
//                calendar.set(Calendar.YEAR, year);
//                calendar.set(Calendar.MONTH, monthOfYear);
//                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                tv_to_date.setText(updateLabel(calendar));
//            }
//        };
//        TextView btnEdit= transactionDialog.findViewById(R.id.btnEdit);
//        btnEdit.setText("Get Statement");
//        btnEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String sDate=tv_start_date.getText().toString();
//                String toDate=tv_to_date.getText().toString();
//
//                if (sDate.equalsIgnoreCase("Select Date")&&toDate.equalsIgnoreCase("Select Date"))
//                    Toast.makeText(MainActivity.this, "Select Date", Toast.LENGTH_SHORT).show();
//                else if (sDate.equalsIgnoreCase("Select Date"))
//                    Toast.makeText(MainActivity.this, "Select Start Date", Toast.LENGTH_SHORT).show();
//                else if (toDate.equalsIgnoreCase("Select Date"))
//                    Toast.makeText(MainActivity.this, "Select To Date", Toast.LENGTH_SHORT).show();
//                else
//                {
//                        getInvestmentList();
//                }
//
//            }
//        });
//        transactionDialog.show();
//
//    }

    private void getInvestmentList() {
        statementPojoList.clear();
        statementBeans.clear();
        open_bal=0;
        closing_bal=0;
        prev_closing=0;
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<RTRStatementListResponse> call=apiInterface.getRTRStatement();
        call.enqueue(new Callback<RTRStatementListResponse>() {
            @Override
            public void onResponse(Call<RTRStatementListResponse> call, Response<RTRStatementListResponse> response) {
                if (response.code()==200) {
                     statementBeans = response.body().getStatement();
                    for (int i = 0; i < statementBeans.size(); i++)
                    {
                        RTRStatementListResponse.StatementBean bean = statementBeans.get(i);
                        if (i == 0 && Integer.parseInt(bean.getStatus()) == 0) {
                            open_bal += Float.parseFloat(bean.getCreditAmount());
                            closing_bal += Float.parseFloat(bean.getCreditAmount());
                        }
                        else if (i == 0 && Integer.parseInt(bean.getStatus()) == 1) {
                            open_bal += Float.parseFloat(bean.getCreditAmount());
                            closing_bal = -1 * (Float.parseFloat(bean.getDebitAmount()));
                        } else if (Integer.parseInt(bean.getStatus()) == 0 && i != 0) {
                            open_bal = prev_closing;
                            closing_bal = prev_closing + Float.parseFloat(bean.getCreditAmount());
                        } else if (Integer.parseInt(bean.getStatus()) == 1 && i != 0) {
                            open_bal = prev_closing;
                            closing_bal = prev_closing - Float.parseFloat(bean.getDebitAmount());
                        }
                        String transaction_status = bean.getTransactionStatus();
                        if (transaction_status.equals("1")) {
                            trans = "Investment";
                            amount = String.valueOf(Float.parseFloat(bean.getCreditAmount()));
                        } else if (transaction_status.equals("2")) {
                            trans = "Expense";
                            amount = String.valueOf(-Float.parseFloat(bean.getDebitAmount()));
                        } else if (transaction_status.equals("3")) {
                            trans = "Withdrawal";
                            amount = String.valueOf(-Float.parseFloat(bean.getDebitAmount()));
                        } else if (transaction_status.equals("4")) {
                            trans = "Profit/Loss";
                            if (bean.getStatus().equals("0"))
                                amount = String.valueOf(Float.parseFloat(bean.getCreditAmount()));
                            else
                                amount = String.valueOf(-Float.parseFloat(bean.getDebitAmount()));
                        }
                        statementPojoList.add(new StatementPojo(bean.getTransactionDate(), amount, trans, String.valueOf(open_bal), String.valueOf(closing_bal)));
                        prev_closing = closing_bal;
                    }
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("statement",(Serializable)statementPojoList);
                    StatementFragment statementFragment=new StatementFragment(MainActivity.this);
                    statementFragment.setArguments(bundle);
                    loadFragment(statementFragment);
//                    transactionDialog.dismiss();
                    dialog.dismiss();
                }


                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(MainActivity.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<RTRStatementListResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
}