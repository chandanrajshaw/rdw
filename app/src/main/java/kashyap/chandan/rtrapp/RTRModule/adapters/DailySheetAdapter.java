package kashyap.chandan.rtrapp.RTRModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.DailySheetListResponse;

public class DailySheetAdapter extends RecyclerView.Adapter<DailySheetAdapter.MyViewHolder> {
    Context context;
    List<DailySheetListResponse.DataBean> dailySheet;
    public DailySheetAdapter(Context context, List<DailySheetListResponse.DataBean> dailySheet) {
        this.context=context;
        this.dailySheet=dailySheet;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.daily_sheet_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_withdrawal.setText(dailySheet.get(position).getWithdrawalTilldate());
holder.tv_closing_bal.setText(dailySheet.get(position).getClosingBalance());
holder.tv_opening_bal.setText(dailySheet.get(position).getOpeningBalance());
        holder.tv_date.setText(dailySheet.get(position).getDailyDate());
        holder.tv_expense.setText(dailySheet.get(position).getExpenseTilldate());
        double pl=Double.parseDouble(dailySheet.get(position).getProfit())-Double.parseDouble(dailySheet.get(position).getLoss());
        if (pl<0)
        {
            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.loss_bg));
            holder.tv_pl_after.setText(String.valueOf(pl));
        }
        else if (pl>0)
        {
            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.profit_bg));
            holder.tv_pl_after.setText(String.valueOf(pl));
        }
        else
        {
            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.no_profit_no_loss_bg));
            holder.tv_pl_after.setText(String.valueOf(pl));
        }

//        if ((dailySheet.get(position).getProfit().equalsIgnoreCase("0.0")||dailySheet.get(position).getProfit().equalsIgnoreCase("0"))&&(dailySheet.get(position).getLoss().equalsIgnoreCase("0.0")||dailySheet.get(position).getLoss().equalsIgnoreCase("0")))
//        {
//            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.no_profit_no_loss_bg));
//            holder.tv_pl_after.setText("0");
//        }
//        else if (!dailySheet.get(position).getProfit().equalsIgnoreCase("0.0")||dailySheet.get(position).getProfit().equalsIgnoreCase("0"))
//        {
//            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.profit_bg));
//            holder.tv_pl_after.setText(dailySheet.get(position).getProfit());
//        }
//        else if (!dailySheet.get(position).getLoss().equalsIgnoreCase("0.0")||dailySheet.get(position).getLoss().equalsIgnoreCase("0"))
//        {
//            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.loss_bg));
//            holder.tv_pl_after.setText(dailySheet.get(position).getLoss());
//        }
    }
    @Override
    public int getItemCount() {
        return dailySheet.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_date,tv_opening_bal,tv_closing_bal, tv_expense,tv_pl_after,tv_withdrawal;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_opening_bal=itemView.findViewById(R.id.tv_opening_bal);
            tv_closing_bal=itemView.findViewById(R.id.tv_closing_bal);
            tv_expense =itemView.findViewById(R.id.tv_expense);
            tv_pl_after=itemView.findViewById(R.id.tv_pl_after);
            tv_withdrawal=itemView.findViewById(R.id.tv_withdrawal);
        }
    }
}
