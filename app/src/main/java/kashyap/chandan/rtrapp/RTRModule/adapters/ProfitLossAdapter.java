package kashyap.chandan.rtrapp.RTRModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.DailySheetListResponse;
import kashyap.chandan.rtrapp.response.rtr.ProfitLossListResponse;

public class ProfitLossAdapter extends RecyclerView.Adapter<ProfitLossAdapter.MyViewHolder> {
    Context context;
    List<ProfitLossListResponse.DataBean> dataBeans;
    public ProfitLossAdapter(Context context, List<ProfitLossListResponse.DataBean> dataBeans) {
        this.context=context;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.profit_loss_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String profit_before=dataBeans.get(position).getProfitBeforecommission();
        String profit_after=dataBeans.get(position).getProfit();
        String loss_before=dataBeans.get(position).getLossBeforecommission();
        String loss_after=dataBeans.get(position).getLoss();
        holder.tv_date.setText(dataBeans.get(position).getDailyDate());
if (profit_before.equalsIgnoreCase("0.0")||!loss_before.equalsIgnoreCase("0.0"))
{
    holder.tv_pl_initial.setBackground(context.getDrawable(R.drawable.loss_bg));
    holder.tv_pl_initial.setText(loss_before);
}
else if (!profit_before.equalsIgnoreCase("0.0")||loss_before.equalsIgnoreCase("0.0"))
{
    holder.tv_pl_initial.setBackground(context.getDrawable(R.drawable.profit_bg));
    holder.tv_pl_initial.setText(profit_before);
}
if (profit_after.equalsIgnoreCase("0.0")||!loss_after.equalsIgnoreCase("0.0"))
        {
            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.loss_bg));
            holder.tv_pl_after.setText(loss_after);
        }
        else if (!profit_after.equalsIgnoreCase("0.0")||loss_after.equalsIgnoreCase("0.0"))
        {
            holder.tv_pl_after.setBackground(context.getDrawable(R.drawable.profit_bg));
            holder.tv_pl_after.setText(profit_after);
        }

    }
    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_date,tv_pl_initial,tv_pl_after;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_pl_after=itemView.findViewById(R.id.tv_pl_after);
            tv_pl_initial=itemView.findViewById(R.id.tv_pl_initial);
            tv_date=itemView.findViewById(R.id.tv_date);

        }
    }
}
