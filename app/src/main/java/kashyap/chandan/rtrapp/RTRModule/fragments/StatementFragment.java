package kashyap.chandan.rtrapp.RTRModule.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.StatementPojo;
import kashyap.chandan.rtrapp.response.rtr.RTRDashboardResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StatementFragment extends Fragment {
    MainActivity dashboard;
    TableLayout tl;
    Dialog dialog;
    Bundle bundle;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    TextView tv_investment,tv_expense,tv_withdrawal,tv_profitloss,tv_available;
    List<StatementPojo> statementPojoList = new ArrayList<>();

    public StatementFragment(MainActivity dashboard) {
        this.dashboard = dashboard;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.statement_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        tl = view.findViewById(R.id.table);
        tv_available=view.findViewById(R.id.tv_available);
        tv_profitloss=view.findViewById(R.id.tv_profitloss);
        tv_withdrawal=view.findViewById(R.id.tv_withdrawal);
        tv_expense=view.findViewById(R.id.tv_expense);
        tv_investment=view.findViewById(R.id.tv_investment);
        dialog = new Dialog(dashboard);
        bundle = getArguments();
        if (bundle != null) {
            statementPojoList.clear();
            statementPojoList = (List<StatementPojo>) bundle.getSerializable("statement");
        } else {
            Toast.makeText(dashboard, "Some Error Occured", Toast.LENGTH_SHORT).show();
        }

//        getStatement();
        dashboard();
    }

    private TextView getTextView(int id, String title, int color, int typeface, int bgColor) {
        TextView tv = new TextView(dashboard);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setTextSize(13.0F);
        tv.setGravity(Gravity.CENTER);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }

    @Override
    public void onResume() {
        super.onResume();
        tl.removeAllViews();
        addData();

    }

    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }

    private TableRow.LayoutParams getLayoutParams() {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }

    public void addData() {

        // int numCompanies = companies.length;
        TableRow tr = new TableRow(dashboard);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "S.No", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "   Date   ", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Opening Bal", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Transaction", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Amount", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Closing Bal", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tl.addView(tr, getTblLayoutParams());
////        tl.removeAllViews();
//        double balance=0;
        int size = statementPojoList.size();
        for (int i = 0; i < size; i++) {
            TableRow trr = new TableRow(dashboard);
            trr.setLayoutParams(getLayoutParams());
            StatementPojo transaction = statementPojoList.get(i);
            trr.addView(getTextView(1, "" + (i + 1), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            trr.addView(getTextView(1,transaction.getDate(), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            trr.addView(getTextView(1,transaction.getOpeningBal(), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            trr.addView(getTextView(1,transaction.getType(), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            trr.addView(getTextView(1, transaction.getAmount(), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            trr.addView(getTextView(1,transaction.getClosingBal(), ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.NORMAL, ContextCompat.getColor(dashboard, R.color.white)));
            tl.addView(trr, getTblLayoutParams());
        }
//    private  void getStatement()
//    {
//        dialog.setContentView(R.layout.loadingdialog);
//        dialog.setCancelable(true);
//        dialog.show();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
//        Call<RTRStatementResponse> call=apiInterface.getstatement();
//        call.enqueue(new Callback<RTRStatementResponse>() {
//            @Override
//            public void onResponse(Call<RTRStatementResponse> call, Response<RTRStatementResponse> response) {
//                if (response.code()==200)
//                {
//                    dialog.dismiss();
//                    String url=response.body().getExcelDailysheet();
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                    startActivity(browserIntent);
//                }
//                else
//                {
//                    dialog.dismiss();
//                    Converter<ResponseBody, ApiError> converter =
//                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
//                    ApiError error;
//                    try {
//                        error = converter.convert(response.errorBody());
//                        ApiError.StatusBean status=error.getStatus();
//                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
//                    } catch (IOException e) { e.printStackTrace(); }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<RTRStatementResponse> call, Throwable t) {
//                dialog.dismiss();
//                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//    }
    }
    private String formatDate(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(date);
    }
    private void dashboard()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<RTRDashboardResponse>call=apiInterface.getRTRResponse();
        call.enqueue(new Callback<RTRDashboardResponse>() {
            @Override
            public void onResponse(Call<RTRDashboardResponse> call, Response<RTRDashboardResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    tv_available.setText(String.valueOf(response.body().getCurrentbalance()));
                    if (response.body().getTotalexpense().getSum()==null)
                        tv_expense.setText("0");
                    else  tv_expense.setText(response.body().getTotalexpense().getSum());
                    if (response.body().getTotalwithdrawal().getSum()==null)
                        tv_withdrawal.setText("0");
                    else   tv_withdrawal.setText(response.body().getTotalwithdrawal().getSum());
                    if (response.body().getTotalinvestment().getSum()==null)
                        tv_investment.setText("0");
                    else  tv_investment.setText(response.body().getTotalinvestment().getSum());
                    if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_profitloss.setText("0");
                    else if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()!=null)
                        tv_profitloss.setText("-"+response.body().getTotalLoss().getLosssum());
                    else if (response.body().getTotalProfit().getProfitsum()!=null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_profitloss.setText(response.body().getTotalProfit().getProfitsum());
                    else
                    {double profit_loss=Double.parseDouble(response.body().getTotalProfit().getProfitsum())-Double.parseDouble(response.body().getTotalLoss().getLosssum());
                        tv_profitloss.setText(String.valueOf(profit_loss));}

                }
                else
                {
                    dialog.dismiss();
                    tv_available.setText("0");
                    tv_expense.setText("0");

                    tv_investment.setText("0");
                    tv_profitloss.setText("0");
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<RTRDashboardResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
