package kashyap.chandan.rtrapp.RTRModule;

import java.io.Serializable;
import java.util.Date;

public class StatementPojo implements Serializable,Comparable<StatementPojo> {
    String amount,type,openingBal,closingBal;
    String date;

    public StatementPojo( String date,String amount, String type, String openingBal, String closingBal) {
        this.amount = amount;
        this.type = type;
        this.openingBal = openingBal;
        this.closingBal = closingBal;
        this.date = date;
    }

    public String getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(String openingBal) {
        this.openingBal = openingBal;
    }

    public String getClosingBal() {
        return closingBal;
    }
    public void setClosingBal(String closingBal) {
        this.closingBal = closingBal;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int compareTo(StatementPojo statementPojo) {
        return getDate().compareTo(statementPojo.getDate());
    }
}
