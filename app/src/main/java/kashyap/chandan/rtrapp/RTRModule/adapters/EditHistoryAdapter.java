package kashyap.chandan.rtrapp.RTRModule.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.ViewInvestmentDetailResponse;

public class EditHistoryAdapter extends RecyclerView.Adapter<EditHistoryAdapter.MyViewHolder>
{
    Context context;
    List<ViewInvestmentDetailResponse.DataBean> history;
    public EditHistoryAdapter(Context context, List<ViewInvestmentDetailResponse.DataBean> history) {
        this.context=context;
        this.history=history;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.edit_history_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.tv_date.setText(history.get(position).getEditInvestdate());
holder.tv_edited_amount.setText(history.get(position).getEditAmount());
holder.tv_reason.setText(history.get(position).getEditReason());

    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_edited_amount,tv_date,tv_reason;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_edited_amount=itemView.findViewById(R.id.tv_edited_amount);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_reason=itemView.findViewById(R.id.tv_reason);
        }
    }
}
