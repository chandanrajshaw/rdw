package kashyap.chandan.rtrapp.RTRModule.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.adapters.ExpenseTypeFragmentAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.EditExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.ExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.rtr.InvestmentListResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ExpenseTypeFragment extends Fragment {
    RecyclerView recyclerView;
    Context context;
    CircleImageView add;
    MainActivity dashboard;
    Calendar myCalendar,calendar;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    List<ExpenseTypeResponse.DataBean>expenseList=new ArrayList<>();
    DatePickerDialog.OnDateSetListener dateDialog;
TextView tvDate;
    List<InvestmentListResponse.DataBean>dataBeans=new ArrayList<>();
Dialog editExpenseTypeDialog, addexpensetypeDialog,dialog;
    public ExpenseTypeFragment(Context context) {
        this.context = context;
        this.dashboard = (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.expense_type_fragment,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        recyclerView=view.findViewById(R.id.recyclerView);
        add=view.findViewById(R.id.add);
        myCalendar=Calendar.getInstance();
        calendar=Calendar.getInstance();
        dialog=new Dialog(dashboard);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
addExpenseTypeDialog();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));


    }

    @Override
    public void onResume() {
        super.onResume();
    getExpenseType();
    }



    private void addExpenseTypeDialog()
    {
        addexpensetypeDialog =new Dialog(dashboard);
        addexpensetypeDialog.setContentView(R.layout.add_expense_type_dialog);
        addexpensetypeDialog.setCancelable(false);
        addexpensetypeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close= addexpensetypeDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addexpensetypeDialog.dismiss();
            }
        });
        TextView header=addexpensetypeDialog.findViewById(R.id.header);
        header.setText("Add Expense Type");
        EditText et_expense_type= addexpensetypeDialog.findViewById(R.id.et_expense_type);
        TextView btnEdit= addexpensetypeDialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String expenseType=et_expense_type.getText().toString().trim();
                 if (expenseType.isEmpty())
                    Toast.makeText(context, "Enter Expense Type", Toast.LENGTH_SHORT).show();
                else
                {
                    addExpenseType(expenseType);
                }

            }
        });
        addexpensetypeDialog.show();

    }
    private void editExpenseTypeDialog(String id,String expense_type)
    {
        editExpenseTypeDialog =new Dialog(dashboard);
        editExpenseTypeDialog.setContentView(R.layout.add_expense_type_dialog);
        editExpenseTypeDialog.setCancelable(false);
        editExpenseTypeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView header=editExpenseTypeDialog.findViewById(R.id.header);
        header.setText("Edit Expense Type");
        ImageView close= editExpenseTypeDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editExpenseTypeDialog.dismiss();
            }
        });
        EditText et_expense_type= editExpenseTypeDialog.findViewById(R.id.et_expense_type);
        et_expense_type.setText(expense_type);
        TextView btnEdit= editExpenseTypeDialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Edit");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String expenseType=et_expense_type.getText().toString().trim();
                if (expenseType.isEmpty())
                    Toast.makeText(context, "Enter Expense Type", Toast.LENGTH_SHORT).show();
                else
                {
                    editExpenseType(id,expenseType);
                }

            }
        });
        editExpenseTypeDialog.show();

    }

    private void editExpenseType(String id, String expenseType) {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<EditExpenseTypeResponse>call=apiInterface.editExpenseType(id,expenseType,"1");
        call.enqueue(new Callback<EditExpenseTypeResponse>() {
            @Override
            public void onResponse(Call<EditExpenseTypeResponse> call, Response<EditExpenseTypeResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashboard, "Updated", Toast.LENGTH_SHORT).show();
                    editExpenseTypeDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<EditExpenseTypeResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addExpenseType(String expenseType)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<AddExpenseTypeResponse>call=apiInterface.addExpenseType(expenseType,"1");
        call.enqueue(new Callback<AddExpenseTypeResponse>() {
            @Override
            public void onResponse(Call<AddExpenseTypeResponse> call, Response<AddExpenseTypeResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashboard, "Added Successfully", Toast.LENGTH_SHORT).show();
                    addexpensetypeDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<AddExpenseTypeResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private  void getExpenseType()
    {

        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        final Call<ExpenseTypeResponse> call=apiInterface.getExpenseTypeList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<ExpenseTypeResponse>() {
                    @Override
                    public void onResponse(Call<ExpenseTypeResponse> call, Response<ExpenseTypeResponse> response) {
                        if (response.code()==200)
                        {
                            dialog.dismiss();

                            expenseList=response.body().getData();
                            recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
                            recyclerView.setAdapter(new ExpenseTypeFragmentAdapter(dashboard,expenseList,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
                                    editExpenseTypeDialog(id,value);

                                }
                            }));
                        }
                        else
                        {
                            dialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<ExpenseTypeResponse> call, Throwable t) {
                        dialog.dismiss();
                        Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }

}
