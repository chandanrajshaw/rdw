package kashyap.chandan.rtrapp.RTRModule.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.LatestInvestmentResponse;

public class LatestInvestmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private static final int TYPE_HEADER=0;
    private static final int TYPE_ITEM=1;
    List<LatestInvestmentResponse.DataBean> investmentLatest;
    public LatestInvestmentAdapter(Context context, List<LatestInvestmentResponse.DataBean> investmentLatest) {
        this.context = context;
        this.investmentLatest=investmentLatest;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==TYPE_ITEM)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.investment_layout,parent,false);
            return new ItemViewHolder(view);
        }
       else if (viewType==TYPE_HEADER)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.investment_header,parent,false);
            return new HeaderViewHolder(view);
        }
       else
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
if (holder instanceof ItemViewHolder)
{
    final ItemViewHolder viewHolder= (ItemViewHolder) holder;
    viewHolder.tv_date.setText(investmentLatest.get(position-1).getDate());
    viewHolder.tv_amount.setText(investmentLatest.get(position-1).getInvestAmount());
    viewHolder.tv_invest_till_date.setText(investmentLatest.get(position-1).getTotalInvest());

}
        else if (holder instanceof HeaderViewHolder)
        {
            final HeaderViewHolder viewHolder= (HeaderViewHolder) holder;
        }
    }

    @Override
    public int getItemCount() {
        if (investmentLatest.size()<10)
            return investmentLatest.size()+1;
        else
        return 11;
        //Size should be +1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
        {
            return TYPE_HEADER;
        }
        return TYPE_ITEM ;
    }
    private class HeaderViewHolder  extends RecyclerView.ViewHolder
    {

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    private class ItemViewHolder  extends RecyclerView.ViewHolder
    {
TextView tv_date,tv_amount,tv_invest_till_date;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_invest_till_date=itemView.findViewById(R.id.tv_invest_till_date);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_amount=itemView.findViewById(R.id.tv_amount);
        }
    }
}
