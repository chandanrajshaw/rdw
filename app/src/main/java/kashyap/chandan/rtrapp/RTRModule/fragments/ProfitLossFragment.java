package kashyap.chandan.rtrapp.RTRModule.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.adapters.ProfitLossAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddProfitLossResponse;
import kashyap.chandan.rtrapp.response.rtr.ProfitLossListResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ProfitLossFragment extends Fragment
{
    RecyclerView recyclerView;
    MainActivity dashboard;
    Context context;
    Calendar calendar;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    Dialog dialog,addwithdrawaldialog,editwithdrawaldialog,addprofit_loss_dialog;
    DatePickerDialog.OnDateSetListener requestedDateDialog,reflectedDateDialog,dateDialog;
    CircleImageView add;
    List<ProfitLossListResponse.DataBean> dataBeans=new ArrayList<>();
    ProfitLossAdapter adapter;
    double initialprofit;
    double initialloss;
    double finalprofit;
    double finalloss;
    public ProfitLossFragment(Context context) {
        this.context = context;
        this.dashboard= (MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.profit_loss_fragment,container,false);
        return view; }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        add=view.findViewById(R.id.add);
        recyclerView=view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));

        dialog=new Dialog(dashboard);
        calendar=Calendar.getInstance();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addProfitLossDiaog();
            }
        });


    }
    private void getProfitLossList()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        Toast.makeText(dashboard, "Done", Toast.LENGTH_SHORT).show();
//        Toast.makeText(dashboard, "InitialProfit"+initiaprofit+"Initial Loss"+initialloss+"final Profit"+finalprofit+"final Loss"+finalloss, Toast.LENGTH_LONG).show();
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<ProfitLossListResponse>call=apiInterface.getProfitLoss();
        call.enqueue(new Callback<ProfitLossListResponse>() {
            @Override
            public void onResponse(Call<ProfitLossListResponse> call, Response<ProfitLossListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    recyclerView.setAdapter(null);
                  dataBeans=response.body().getData();
                 adapter=new ProfitLossAdapter(dashboard,dataBeans);
                 recyclerView.setAdapter(adapter);
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<ProfitLossListResponse> call, Throwable t) {

            }
        });
    }

    private void addProfitLossDiaog()
    {
        addprofit_loss_dialog =new Dialog(dashboard);
        addprofit_loss_dialog.setContentView(R.layout.add_profit_loss_dialog);
        addprofit_loss_dialog.setCancelable(false);
        addprofit_loss_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close= addprofit_loss_dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addprofit_loss_dialog.dismiss();
            }
        });
        TextView tvDate= addprofit_loss_dialog.findViewById(R.id.tv_date);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog= new DatePickerDialog(getContext(),R.style.TimePickerTheme,dateDialog,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
//              datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
                datePickerDialog.show();
            }
        });
        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvDate.setText(updateLabel(calendar));
            }
        };

        RadioGroup grp_1= addprofit_loss_dialog.findViewById(R.id.grp_1);
        RadioGroup grp_2= addprofit_loss_dialog.findViewById(R.id.grp_2);
        RadioButton profit_initial= addprofit_loss_dialog.findViewById(R.id.profit_initial);
        RadioButton loss_initial= addprofit_loss_dialog.findViewById(R.id.loss_initial);
        RadioButton profit_after= addprofit_loss_dialog.findViewById(R.id.profit_after);
        RadioButton loss_after= addprofit_loss_dialog.findViewById(R.id.loss_after);
        EditText et_initial_amount= addprofit_loss_dialog.findViewById(R.id.et_initial_amount);
        EditText et_final_amount= addprofit_loss_dialog.findViewById(R.id.et_final_amount);
        TextView btnEdit= addprofit_loss_dialog.findViewById(R.id.btnEdit);

        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int before=grp_1.getCheckedRadioButtonId();
                int after=grp_2.getCheckedRadioButtonId();
                String date=tvDate.getText().toString();
                String final_amt=et_final_amount.getText().toString();

                String initial_amt=et_initial_amount.getText().toString().trim();
                if (date.equalsIgnoreCase("Select Date")&&initial_amt.isEmpty()&&final_amt.isEmpty())
                    Toast.makeText(context, "Enter All fields", Toast.LENGTH_SHORT).show();
                else if (!(profit_initial.isChecked()||loss_initial.isChecked()))
                    Toast.makeText(dashboard, "Please Select Profit or loss", Toast.LENGTH_SHORT).show();
                else if (!(profit_after.isChecked()||loss_after.isChecked()))
                    Toast.makeText(dashboard, "Please Select Profit or loss", Toast.LENGTH_SHORT).show();
                else if (date.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Choose Date", Toast.LENGTH_SHORT).show();
                else if (initial_amt.isEmpty())
                    Toast.makeText(context, "Enter Initial Profit/Loss", Toast.LENGTH_SHORT).show();
                else if (final_amt.isEmpty())
                    Toast.makeText(context, "Enter Final Profit/Loss", Toast.LENGTH_SHORT).show();
                else
                {
                    if (before==R.id.profit_initial)
                    {
                        initialprofit=Double.parseDouble(initial_amt);
                        initialloss=0;
                    }
                    else if (before==R.id.loss_initial)
                    {
                        initialloss=Double.parseDouble(initial_amt);
                        initialprofit=0;
                    }
                    else
                    {
                        Toast.makeText(dashboard, "Check Initial Profit or Loss", Toast.LENGTH_SHORT).show();
                    }
                    if (after==R.id.profit_after)
                    {
                        finalprofit=Double.parseDouble(final_amt);
                        finalloss=0;
                    }
                    else if (after==R.id.loss_after)
                    {
                        finalprofit=0;
                        finalloss=Double.parseDouble(final_amt);;
                    }
                    else
                    {
                        Toast.makeText(dashboard, "Check Final Profit or Loss", Toast.LENGTH_SHORT).show();
                    }
                    add_Profit_Loss(date,initialprofit,initialloss,finalprofit,finalloss);
                }
            }
        });
        addprofit_loss_dialog.show();

    }
    private void add_Profit_Loss(String date, double initiaprofit, double initialloss, double finalprofit, double finalloss)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        Toast.makeText(dashboard, "Done", Toast.LENGTH_SHORT).show();
//        Toast.makeText(dashboard, "InitialProfit"+initiaprofit+"Initial Loss"+initialloss+"final Profit"+finalprofit+"final Loss"+finalloss, Toast.LENGTH_LONG).show();
        APIInterface apiInterface=retrofit.create(APIInterface.class);
        Call<AddProfitLossResponse>call=apiInterface.addProfitLoss(String.valueOf(date),
                String.valueOf(initiaprofit),
                String.valueOf(initialloss),
                String.valueOf(finalprofit),
                String.valueOf(finalloss));
        call.enqueue(new Callback<AddProfitLossResponse>() {
            @Override
            public void onResponse(Call<AddProfitLossResponse> call, Response<AddProfitLossResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    addprofit_loss_dialog.dismiss();
                    Toast.makeText(dashboard, "Added", Toast.LENGTH_SHORT).show();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<AddProfitLossResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
getProfitLossList();
    }


    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
}
