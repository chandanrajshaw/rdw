package kashyap.chandan.rtrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;

import javax.inject.Inject;

import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.response.rtr.RTRDashboardResponse;
import kashyap.chandan.rtrapp.response.str.STRDashboardResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiClientComponent;
import kashyap.chandan.rtrapp.retrofit.ApiClientModule;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import kashyap.chandan.rtrapp.retrofit.DaggerApiClientComponent;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RDWDashboard extends AppCompatActivity {
ConstraintLayout rtr_layout,str_layout;
TextView tv_rtr_total_investment,tv_rtr_profit_loss,tv_rtr_total_withdrawal,tv_rtr_total_available;
TextView tv_str_total_investment,tv_str_profit_loss,tv_str_total_withdrawal,tv_str_total_available;
Dialog dialog;
    @Inject
    Retrofit retrofit;
    ApiClientComponent apiClientComponent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rdw_dashboard);
        init();
        apiClientComponent= DaggerApiClientComponent.builder().apiClientModule(new ApiClientModule()).build();
        apiClientComponent.inject(this);
        rtr_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RDWDashboard.this, MainActivity.class);
                startActivity(intent);
            }
        });
        str_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RDWDashboard.this, STRDashboard.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        RTRdashboard();
        STRdashboard();
    }

    private void init() {

        tv_str_total_investment=findViewById(R.id.tv_str_total_investment);
        tv_str_profit_loss=findViewById(R.id.tv_str_profit_loss);
        tv_str_total_withdrawal=findViewById(R.id.tv_str_total_withdrawal);
        tv_str_total_available=findViewById(R.id.tv_str_total_available);

        tv_rtr_total_investment=findViewById(R.id.tv_rtr_total_investment);
        tv_rtr_profit_loss=findViewById(R.id.tv_rtr_profit_loss);
        tv_rtr_total_withdrawal=findViewById(R.id.tv_rtr_total_withdrawal);
        tv_rtr_total_available=findViewById(R.id.tv_rtr_total_available);
        str_layout=findViewById(R.id.str_layout);
        rtr_layout=findViewById(R.id.rtr_layout);
        dialog=new Dialog(RDWDashboard.this);
    }
    private void RTRdashboard()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=retrofit.create(APIInterface.class);
        Call<RTRDashboardResponse>call=apiInterface.getRTRResponse();
        call.enqueue(new Callback<RTRDashboardResponse>() {
            @Override
            public void onResponse(Call<RTRDashboardResponse> call, Response<RTRDashboardResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    tv_rtr_total_available.setText(String.valueOf(response.body().getCurrentbalance()));
//                    if (response.body().getTotalexpense().getSum()==null)
//                        tv_expense_count.setText("0");
//                    else  tv_expense_count.setText(response.body().getTotalexpense().getSum());
                    if (response.body().getTotalwithdrawal().getSum()==null)
                        tv_rtr_total_withdrawal.setText("0");
                    else   tv_rtr_total_withdrawal.setText(response.body().getTotalwithdrawal().getSum());
                    if (response.body().getTotalinvestment().getSum()==null)
                        tv_rtr_total_investment.setText("0");
                    else  tv_rtr_total_investment.setText(response.body().getTotalinvestment().getSum());
                    if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_rtr_profit_loss.setText("0");
                    else if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()!=null)
                        tv_rtr_profit_loss.setText("-"+response.body().getTotalLoss().getLosssum());
                    else if (response.body().getTotalProfit().getProfitsum()!=null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_rtr_profit_loss.setText(response.body().getTotalProfit().getProfitsum());
                    else
                    {double profit_loss=Double.parseDouble(response.body().getTotalProfit().getProfitsum())-Double.parseDouble(response.body().getTotalLoss().getLosssum());
                        tv_rtr_profit_loss.setText(String.valueOf(profit_loss));}

                }
                else
                {
                    dialog.dismiss();
                    tv_rtr_total_available.setText("0");
//                    tv_expense_count.setText("0");
                    tv_rtr_total_investment.setText("0");
                    tv_rtr_profit_loss.setText("0");
                    tv_rtr_total_withdrawal.setText("0");
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(RDWDashboard.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<RTRDashboardResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(RDWDashboard.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void STRdashboard()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= retrofit.create(APIInterface.class);
        Call<STRDashboardResponse> call=apiInterface.getSTRResponse();
        call.enqueue(new Callback<STRDashboardResponse>() {
            @Override
            public void onResponse(Call<STRDashboardResponse> call, Response<STRDashboardResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    tv_str_total_available.setText(String.valueOf(response.body().getCurrentbalance()));
//                    if (response.body().getTotalexpense().getSum()==null)
//                        tv_expense_count.setText("0");
//                    else  tv_expense_count.setText(response.body().getTotalexpense().getSum());
                    if (response.body().getTotalwithdrawal().getSum()==null)
                        tv_str_total_withdrawal.setText("0");
                    else   tv_str_total_withdrawal.setText(response.body().getTotalwithdrawal().getSum());
                    if (response.body().getTotalinvestment().getSum()==null)
                        tv_str_total_investment.setText("0");
                    else  tv_str_total_investment.setText(response.body().getTotalinvestment().getSum());
                    if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_str_profit_loss.setText("0");
                    else if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()!=null)
                        tv_str_profit_loss.setText("-"+response.body().getTotalLoss().getLosssum());
                    else if (response.body().getTotalProfit().getProfitsum()!=null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_str_profit_loss.setText(response.body().getTotalProfit().getProfitsum());
                    else
                    {double profit_loss=Double.parseDouble(response.body().getTotalProfit().getProfitsum())-Double.parseDouble(response.body().getTotalLoss().getLosssum());
                        tv_str_profit_loss.setText(String.valueOf(profit_loss));}
                }
                else
                {
                    dialog.dismiss();
                    tv_str_total_available.setText("0");
                    tv_str_total_withdrawal.setText("0");
                    tv_str_total_investment.setText("0");
                    tv_str_profit_loss.setText("0");
                    Converter<ResponseBody, ApiError> converter =
                           retrofit.responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(RDWDashboard.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRDashboardResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(RDWDashboard.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}