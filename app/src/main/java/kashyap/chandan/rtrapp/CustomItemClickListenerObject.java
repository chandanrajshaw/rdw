package kashyap.chandan.rtrapp;

import android.view.View;

public interface CustomItemClickListenerObject {
    public void onItemClickListener(View v,Object object);
}
