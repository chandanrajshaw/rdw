package kashyap.chandan.rtrapp.STRModule;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.rtrapp.ChangePin;
import kashyap.chandan.rtrapp.LoginScreen;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.fragments.DailySheetFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ExpenseTypeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.HomeFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.InvestmentFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.ProfitLossFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.StatementFragment;
import kashyap.chandan.rtrapp.RTRModule.fragments.WithdrawalFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRDailySheetFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRExpenseFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRExpenseTypeFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRHomeFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRInvestmentFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRProfitLossFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRStatementFragment;
import kashyap.chandan.rtrapp.STRModule.strFragment.STRWithdrawalFragment;
import kashyap.chandan.rtrapp.SharedPreferenceData;
import kashyap.chandan.rtrapp.response.rtr.RTRStatementResponse;
import kashyap.chandan.rtrapp.response.str.STRStatementResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRDashboard extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    ImageView iv_menu;
    TextView tv_logout,tv_header;
    Dialog dialog;
    SharedPreferenceData preferenceData;
    RelativeLayout nav_statement_layout,nav_profit_loss_layout,nav_ecpense_type_layout,nav_change_pin_layout,nav_expense_layout,nav_withdraw_layout,nav_home_layout,nav_investment_layout,nav_daily_sheet_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        tv_logout.setOnClickListener(this);
        nav_statement_layout.setOnClickListener(this);
        nav_profit_loss_layout.setOnClickListener(this);
        nav_ecpense_type_layout.setOnClickListener(this);
        nav_change_pin_layout.setOnClickListener(this);
        nav_expense_layout.setOnClickListener(this);
        nav_withdraw_layout.setOnClickListener(this);
        nav_home_layout.setOnClickListener(this);
        nav_investment_layout.setOnClickListener(this);
        nav_daily_sheet_layout.setOnClickListener(this);
        tv_header.setText("STR");
        STRHomeFragment strHomeFragment=new STRHomeFragment(STRDashboard.this);
        loadFragment(strHomeFragment);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
                else
                {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    private void init() {
        dialog=new Dialog(STRDashboard.this);
        nav_profit_loss_layout=findViewById(R.id.nav_profit_loss_layout);
        tv_header=findViewById(R.id.tv_header);
        preferenceData=new SharedPreferenceData(STRDashboard.this);
        tv_logout=findViewById(R.id.tv_logout);
        nav_ecpense_type_layout=findViewById(R.id.nav_ecpense_type_layout);
        nav_change_pin_layout=findViewById(R.id.nav_change_pin_layout);
        nav_investment_layout=findViewById(R.id.nav_investment_layout);
        nav_home_layout=findViewById(R.id.nav_home_layout);
        nav_withdraw_layout=findViewById(R.id.nav_withdraw_layout);
        nav_expense_layout=findViewById(R.id.nav_expense_layout);
        nav_daily_sheet_layout=findViewById(R.id.nav_daily_sheet_layout);
        iv_menu=findViewById(R.id.iv_menu);
        drawerLayout=findViewById(R.id.drawerLayout);
        nav_statement_layout=findViewById(R.id.nav_statement_layout);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.nav_home_layout:
                STRHomeFragment strHomeFragment=new STRHomeFragment(STRDashboard.this);
                loadFragment(strHomeFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_expense_layout:
              STRExpenseFragment strExpenseFragment=new STRExpenseFragment(STRDashboard.this);
                loadFragment(strExpenseFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_withdraw_layout:
                STRWithdrawalFragment strWithdrawalFragment=new STRWithdrawalFragment(STRDashboard.this);
                loadFragment(strWithdrawalFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_investment_layout:
                STRInvestmentFragment strInvestmentFragment=new STRInvestmentFragment(STRDashboard.this);
                loadFragment(strInvestmentFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_daily_sheet_layout:
                STRDailySheetFragment strDailySheetFragment=new STRDailySheetFragment(STRDashboard.this);
                loadFragment(strDailySheetFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_change_pin_layout:
                Intent intent=new Intent(STRDashboard.this, ChangePin.class);
                startActivity(intent);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case  R.id.tv_logout:
                Intent intent1=new Intent(STRDashboard.this, LoginScreen.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                preferenceData.sessionEnd();
                startActivity(intent1);
                finish();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_ecpense_type_layout:
                STRExpenseTypeFragment strExpenseTypeFragment=new STRExpenseTypeFragment(STRDashboard.this);
                loadFragment(strExpenseTypeFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_profit_loss_layout:
                STRProfitLossFragment profitLossFragment=new STRProfitLossFragment(STRDashboard.this);
                loadFragment(profitLossFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_statement_layout:
                STRStatementFragment strStatementFragment=new STRStatementFragment(STRDashboard.this);
                loadFragment(strStatementFragment);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
        }
    }
    private  void getStatement()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRStatementResponse> call=apiInterface.getSTRstatement();
        call.enqueue(new Callback<STRStatementResponse>() {
            @Override
            public void onResponse(Call<STRStatementResponse> call, Response<STRStatementResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    String url=response.body().getExcelStrdailysheet();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(STRDashboard.this, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRStatementResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(STRDashboard.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
   private void loadFragment(Fragment fragment)
    {
        if (fragment!=null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.main_frame, fragment);
            ft.commit();
        }
    }

}