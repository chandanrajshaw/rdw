package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.adapters.InvestAdapter;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.STRModule.strAdapter.STRInvestAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.EditInvestmentResponse;
import kashyap.chandan.rtrapp.response.rtr.InvestmentListResponse;
import kashyap.chandan.rtrapp.response.str.EditSTRInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.STRAddInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.STRInvestmentListResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRInvestmentFragment extends Fragment {
    RecyclerView recyclerView;
    Context context;
    CircleImageView add;
    STRDashboard dashboard;
    Calendar myCalendar,calendar;
    DatePickerDialog.OnDateSetListener dateDialog;
TextView tvDate;
    List<STRInvestmentListResponse.DataBean>dataBeans=new ArrayList<>();
Dialog editInvestmentDialog,addInvestmentDialog,dialog;
    public STRInvestmentFragment(Context context) {
        this.context = context;
        this.dashboard = (STRDashboard) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.investment_fragment,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.recyclerView);
        add=view.findViewById(R.id.add);
        myCalendar=Calendar.getInstance();
        calendar=Calendar.getInstance();
        dialog=new Dialog(dashboard);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
addInvestmentDialog();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));


    }

    @Override
    public void onResume() {
        super.onResume();
        getInvestments();
    }

    private void editInvestmentDialog(String id, String amount)
    {
        editInvestmentDialog=new Dialog(dashboard);
        editInvestmentDialog.setContentView(R.layout.edit_investment_dialog);
        editInvestmentDialog.setCancelable(false);
        editInvestmentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close=editInvestmentDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editInvestmentDialog.dismiss();
            }
        });
       tvDate=editInvestmentDialog.findViewById(R.id.tv_date);
        EditText et_amount=editInvestmentDialog.findViewById(R.id.et_initial_amount);
        et_amount.setText(amount);

        EditText et_reason=editInvestmentDialog.findViewById(R.id.et_reason);
        TextView btnEdit=editInvestmentDialog.findViewById(R.id.btnEdit);
        tvDate.setText(updateLabel(myCalendar));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String reason=et_reason.getText().toString().trim();
                String amt=et_amount.getText().toString().trim();
                String date=tvDate.getText().toString();
                double checkamt=Double.parseDouble(amt);
                if (reason.isEmpty()&&amt.isEmpty())
                    Toast.makeText(context, "Fill All the Fields", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty()||checkamt<=0.0)
                    Toast.makeText(context, "Enter Amount", Toast.LENGTH_SHORT).show();
                else if (reason.isEmpty())
                    Toast.makeText(context, "Enter Reason", Toast.LENGTH_SHORT).show();
               else
                {
                editInvestment(id,date,amt,reason);
                }

            }
        });
        editInvestmentDialog.show();

    }
    private void editInvestment(String id,String date,String amount,String reason)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<EditSTRInvestmentResponse>call=apiInterface.editSTRInvestment(id,date,amount,reason);
        call.enqueue(new Callback<EditSTRInvestmentResponse>() {
            @Override
            public void onResponse(Call<EditSTRInvestmentResponse> call, Response<EditSTRInvestmentResponse> response) {
                if (response.code()==200)
                {
                 dialog.dismiss();
                    Toast.makeText(dashboard, "Investment Edited Successfully", Toast.LENGTH_SHORT).show();
                    editInvestmentDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<EditSTRInvestmentResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void addInvestmentDialog()
    {
        addInvestmentDialog=new Dialog(dashboard);
        addInvestmentDialog.setContentView(R.layout.add_investment_dialog);
        addInvestmentDialog.setCancelable(false);
        addInvestmentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close=addInvestmentDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addInvestmentDialog.dismiss();
            }
        });
       TextView tvDate=addInvestmentDialog.findViewById(R.id.tv_date);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme,dateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvDate.setText(updateLabel(calendar));
            }
        };
        EditText et_amount=addInvestmentDialog.findViewById(R.id.et_initial_amount);
        TextView btnEdit=addInvestmentDialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date=tvDate.getText().toString();
                String amt=et_amount.getText().toString().trim();
                double checkamt=Double.parseDouble(amt);
                if (date.equalsIgnoreCase("Select Date")&&amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else if (date.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Date", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty()||checkamt<=0.0)
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else
                {
                    addInvestment(date,amt);
                }

            }
        });
        addInvestmentDialog.show();

    }
    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
    private void addInvestment(String date,String amount)
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRAddInvestmentResponse>call=apiInterface.addSTRInvestment(date,amount);
        call.enqueue(new Callback<STRAddInvestmentResponse>() {
            @Override
            public void onResponse(Call<STRAddInvestmentResponse> call, Response<STRAddInvestmentResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashboard, "Added Successfully", Toast.LENGTH_SHORT).show();
                    addInvestmentDialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRAddInvestmentResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getInvestments()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRInvestmentListResponse>call=apiInterface.getSTRInvestments();
        call.enqueue(new Callback<STRInvestmentListResponse>() {
            @Override
            public void onResponse(Call<STRInvestmentListResponse> call, Response<STRInvestmentListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                   dataBeans=response.body().getData();
                    recyclerView.setAdapter(new STRInvestAdapter(dashboard,dataBeans,new CustomItemClickListener() {
                        @Override
                        public void onItemClick(View v, String id, String amount) {
                            editInvestmentDialog(id,amount);
                        }
                    }));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRInvestmentListResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
