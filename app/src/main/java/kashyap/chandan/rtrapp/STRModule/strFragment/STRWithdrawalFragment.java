package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.CustomItemClickListenerObject;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.adapters.WithdrawalAdapter;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.STRModule.strAdapter.STRWithdrawalAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddWithdrawalResponse;
import kashyap.chandan.rtrapp.response.rtr.EditWithdrawalResponse;
import kashyap.chandan.rtrapp.response.rtr.WithdrawalListRsponse;
import kashyap.chandan.rtrapp.response.str.AddSTRWithdrawalResponse;
import kashyap.chandan.rtrapp.response.str.EditSTRWithdrawalResponse;
import kashyap.chandan.rtrapp.response.str.STRWithdrawalListRsponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRWithdrawalFragment extends Fragment
{
    RecyclerView recyclerView;
    STRDashboard dashboard;
    Context context;
    Calendar calendar;
    Dialog dialog,addwithdrawaldialog,editwithdrawaldialog;
    DatePickerDialog.OnDateSetListener requestedDateDialog,reflectedDateDialog;
    CircleImageView add;
    List<STRWithdrawalListRsponse.DataBean> dataBeans=new ArrayList<>();
    public STRWithdrawalFragment(Context context) {
        this.context = context;
        this.dashboard= (STRDashboard) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.withdrawal_fragment,container,false);
        return view; }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        add=view.findViewById(R.id.add);
        recyclerView=view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
        dialog=new Dialog(dashboard);
        calendar=Calendar.getInstance();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addwithdrawalDialog();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getWithrawal();
    }

    private void getWithrawal()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRWithdrawalListRsponse> call=apiInterface.getSTRWithdrawals();
        call.enqueue(new Callback<STRWithdrawalListRsponse>() {
            @Override
            public void onResponse(Call<STRWithdrawalListRsponse> call, Response<STRWithdrawalListRsponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    dataBeans=response.body().getData();
                    recyclerView.setAdapter(new STRWithdrawalAdapter(dashboard,dataBeans, new CustomItemClickListenerObject() {
                        @Override
                        public void onItemClickListener(View v, Object object) {
                            STRWithdrawalListRsponse.DataBean data= (STRWithdrawalListRsponse.DataBean) object;
                            editwithdrawalDialog(data);
                        }
                    }));

                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRWithdrawalListRsponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void addwithdrawalDialog()
    {
        addwithdrawaldialog=new Dialog(dashboard);
        addwithdrawaldialog.setContentView(R.layout.add_withdrawal_dialog);
        addwithdrawaldialog.setCancelable(false);
        addwithdrawaldialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TextView header=addwithdrawaldialog.findViewById(R.id.header);
        header.setText("Add Withdrawal");
        ImageView close=addwithdrawaldialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addwithdrawaldialog.dismiss();
            }
        });
        TextView tv_requested_date=addwithdrawaldialog.findViewById(R.id.tv_requested_date);
        TextView tv_reflected_date=addwithdrawaldialog.findViewById(R.id.tv_reflected_date);
        tv_reflected_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme, reflectedDateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tv_requested_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme, requestedDateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        requestedDateDialog =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tv_requested_date.setText(updateLabel(calendar));
            }
        };
        reflectedDateDialog =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tv_reflected_date.setText(updateLabel(calendar));
            }
        };
        EditText et_amount=addwithdrawaldialog.findViewById(R.id.et_initial_amount);
        TextView btnEdit=addwithdrawaldialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String requestdate=tv_requested_date.getText().toString();
                String reflctdate=tv_reflected_date.getText().toString();
                String amt=et_amount.getText().toString().trim();

                if (requestdate.equalsIgnoreCase("Select Date")&&reflctdate.equalsIgnoreCase("Select Date")&&amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else if (requestdate.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Request Date", Toast.LENGTH_SHORT).show();
                else if (reflctdate.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Reflect Date", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else
                {
                    double checkamt=Double.parseDouble(amt);
                    if (checkamt<=0)
                        Toast.makeText(dashboard, "Enter Amount Greater than 0", Toast.LENGTH_SHORT).show();
                    else
                    addWithdraw(requestdate,reflctdate,amt);
                }

            }
        });
        addwithdrawaldialog.show();

    }
    private void editwithdrawalDialog(STRWithdrawalListRsponse.DataBean data)
    {
        editwithdrawaldialog=new Dialog(dashboard);
        editwithdrawaldialog.setContentView(R.layout.add_withdrawal_dialog);
        editwithdrawaldialog.setCancelable(false);
        editwithdrawaldialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView close=editwithdrawaldialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editwithdrawaldialog.dismiss();
            }
        });
        TextView header=editwithdrawaldialog.findViewById(R.id.header);
        header.setText("Edit Withdrawal");
        TextView tv_requested_date=editwithdrawaldialog.findViewById(R.id.tv_requested_date);
        tv_requested_date.setText(data.getRequestedDate());
        TextView tv_reflected_date=editwithdrawaldialog.findViewById(R.id.tv_reflected_date);
        tv_reflected_date.setText(data.getReflectedDate());

        tv_reflected_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme, reflectedDateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tv_requested_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme, requestedDateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        requestedDateDialog =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tv_requested_date.setText(updateLabel(calendar));
            }
        };
        reflectedDateDialog =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tv_reflected_date.setText(updateLabel(calendar));
            }
        };
        EditText et_amount=editwithdrawaldialog.findViewById(R.id.et_initial_amount);
        et_amount.setText(data.getAmount());
        TextView btnEdit=editwithdrawaldialog.findViewById(R.id.btnEdit);
        btnEdit.setText("Edit");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String requestdate=tv_requested_date.getText().toString();
                String reflctdate=tv_reflected_date.getText().toString();
                String amt=et_amount.getText().toString().trim();

                if (requestdate.equalsIgnoreCase("Select Date")&&reflctdate.equalsIgnoreCase("Select Date")&&amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else if (requestdate.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Request Date", Toast.LENGTH_SHORT).show();
                else if (reflctdate.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Select Reflect Date", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty())
                    Toast.makeText(context, "Enter Valid Amount", Toast.LENGTH_SHORT).show();
                else
                {
                    double checkamt=Double.parseDouble(amt);
                    if (checkamt<=0.0)
                        Toast.makeText(dashboard, "Enter Amount greater than 0", Toast.LENGTH_SHORT).show();
                    else
                    editWithdraw(data.getId(),requestdate,reflctdate,amt);
                }

            }
        });
        editwithdrawaldialog.show();

    }

    private void addWithdraw(String requestdate, String reflctdate, String amt) {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<AddSTRWithdrawalResponse>call=apiInterface.addSTRWithdrawal(requestdate,reflctdate,amt);
        call.enqueue(new Callback<AddSTRWithdrawalResponse>() {
            @Override
            public void onResponse(Call<AddSTRWithdrawalResponse> call, Response<AddSTRWithdrawalResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    addwithdrawaldialog.dismiss();
                    Toast.makeText(dashboard, "Withdraw Request Added", Toast.LENGTH_SHORT).show();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<AddSTRWithdrawalResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void editWithdraw(String id, String requestdate, String reflctdate, String amt) {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<EditSTRWithdrawalResponse>call=apiInterface.editSTRWithdrawal(id,requestdate,reflctdate,amt);
        call.enqueue(new Callback<EditSTRWithdrawalResponse>() {
            @Override
            public void onResponse(Call<EditSTRWithdrawalResponse> call, Response<EditSTRWithdrawalResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    Toast.makeText(dashboard, "Withdraw Edited Successfully", Toast.LENGTH_SHORT).show();
                    editwithdrawaldialog.dismiss();
                    onResume();
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<EditSTRWithdrawalResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
}
