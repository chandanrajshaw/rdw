package kashyap.chandan.rtrapp.STRModule.strAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.ExpenseListResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseListResponse;

public class STRExpenseAdapter extends RecyclerView.Adapter<STRExpenseAdapter.MyViewHolder> {
    Context context;
    List<STRExpenseListResponse.DataBean> allexpenseList;
    public STRExpenseAdapter(Context context,  List<STRExpenseListResponse.DataBean> allexpenseList) {
        this.context=context;
        this.allexpenseList=allexpenseList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.expense_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        double available=Double.parseDouble(allexpenseList.get(position).getInitialBal())-Double.parseDouble(String.valueOf(allexpenseList.get(position).getExpense()));
        holder.tv_avail_bal.setText(String.valueOf(available));
        holder.tv_date.setText(allexpenseList.get(position).getDate());
        holder.tv_initial.setText(allexpenseList.get(position).getInitialBal());
        holder.tv_expense_id.setText("EXP"+String.valueOf(position));
        holder.tv_expense.setText(String.valueOf(allexpenseList.get(position).getExpense()));
    }

    @Override
    public int getItemCount() {
        return allexpenseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_expense_id,tv_initial,tv_date,tv_avail_bal,tv_expense;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_expense=itemView.findViewById(R.id.tv_expense);
            tv_avail_bal=itemView.findViewById(R.id.tv_avail_bal);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_initial=itemView.findViewById(R.id.tv_initial);
            tv_expense_id=itemView.findViewById(R.id.tv_expense_id);
        }
    }
}
