package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.adapters.ExpenseAdapter;
import kashyap.chandan.rtrapp.RTRModule.adapters.ExpenseTypeAdapter;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.STRModule.strAdapter.STRExpenseAdapter;
import kashyap.chandan.rtrapp.STRModule.strAdapter.STRExpenseTypeAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddExpenseResponse;
import kashyap.chandan.rtrapp.response.rtr.ExpenseListResponse;
import kashyap.chandan.rtrapp.response.rtr.ExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRExpenseResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseListResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseTypeResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRExpenseFragment extends Fragment {
    RecyclerView recyclerView;
    Context context;
    STRDashboard dashboard;
    CircleImageView add;
    TextView tv_expense_type;
    Calendar calendar;
    RecyclerView expenseTypeRecycler;
    String expenseId;
Dialog addExpenseDialog,dialog;
    List< STRExpenseTypeResponse.DataBean>expenseList=new ArrayList<>();
    DatePickerDialog.OnDateSetListener dateDialog;
    List<STRExpenseListResponse.DataBean> allexpenseList=new ArrayList<>();

    public STRExpenseFragment(Context context) {
        this.context = context;
        this.dashboard = (STRDashboard) context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.expense_fragment,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.recyclerView);
        add=view.findViewById(R.id.add);
        dialog=new Dialog(dashboard);
        calendar=Calendar.getInstance();
        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addExpenseDialog();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getExpenseList();
    }

    private void addExpenseDialog()
    {
        addExpenseDialog =new Dialog(dashboard);
        addExpenseDialog.setContentView(R.layout.add_expense_dialog);
        addExpenseDialog.setCancelable(false);
        addExpenseDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
         tv_expense_type=addExpenseDialog.findViewById(R.id.tv_expense_type);
        tv_expense_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            getExpenseType();
            }
        });
        ImageView close= addExpenseDialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addExpenseDialog.dismiss();
            }
        });
        TextView tvDate= addExpenseDialog.findViewById(R.id.tv_date);
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),R.style.TimePickerTheme,dateDialog, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tvDate.setText(updateLabel(calendar));
            }
        };
        EditText et_amount= addExpenseDialog.findViewById(R.id.et_initial_amount);
        TextView btnEdit= addExpenseDialog.findViewById(R.id.btnEdit);
        EditText et_description=addExpenseDialog.findViewById(R.id.et_final_amount);
        btnEdit.setText("Add");
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date=tvDate.getText().toString();
                String description=et_description.getText().toString();
                String amt=et_amount.getText().toString().trim();
                if (date.equalsIgnoreCase("Select Date")&&amt.isEmpty()&&description.isEmpty())
                    Toast.makeText(context, "Enter All fields", Toast.LENGTH_SHORT).show();
                else if (date.equalsIgnoreCase("Select Date"))
                    Toast.makeText(context, "Choose Date", Toast.LENGTH_SHORT).show();
                else if (amt.isEmpty())
                    Toast.makeText(context, "Enter Amount", Toast.LENGTH_SHORT).show();
                else if (description.isEmpty())
                    Toast.makeText(context, "Enter Description", Toast.LENGTH_SHORT).show();
                else
                {
                    addExpense(date,amt,description);
                }
            }
        });
        addExpenseDialog.show();

    }
    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }
    private  void getExpenseType()
    {

        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        final Call<STRExpenseTypeResponse> call=apiInterface.getSTRExpenseTypeList();
        Runnable runnable=new Runnable() {
            @Override
            public void run() {
                call.enqueue(new Callback<STRExpenseTypeResponse>() {
                    @Override
                    public void onResponse(Call<STRExpenseTypeResponse> call, Response<STRExpenseTypeResponse> response) {
                        if (response.code()==200)
                        {
                            dialog.dismiss();
                            final Dialog expenseDialog=new Dialog(dashboard);
                            expenseDialog.setContentView(R.layout.recyclerdialog);
                            expenseDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            expenseTypeRecycler=expenseDialog.findViewById(R.id.recycleroption);
                            expenseDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            expenseList=response.body().getData();
                            expenseTypeRecycler.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
                            expenseTypeRecycler.setAdapter(new STRExpenseTypeAdapter(dashboard,expenseList,expenseDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, String id, String value) {
//
                                    expenseId=id;
                                    tv_expense_type.setText(value);

                                }
                            }));
                            expenseDialog.show();
                        }
                        else
                        {
                            dialog.dismiss();
                            Converter<ResponseBody, ApiError> converter =
                                    ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                        }
                    }

                    @Override
                    public void onFailure(Call<STRExpenseTypeResponse> call, Throwable t) {
dialog.dismiss();
                        Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        Thread thread=new Thread(runnable);
        thread.start();
    }
   private void addExpense(String date, String amt, String description)
   {
     if (expenseId==null||expenseId.isEmpty())
     {
         Toast.makeText(context, "Select Expense Type", Toast.LENGTH_SHORT).show();
     }
     else
     {
         dialog.setContentView(R.layout.loadingdialog);
         dialog.setCancelable(false);
         dialog.show();
         dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
         APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
         Call<AddSTRExpenseResponse>call=apiInterface.addSTRExpense(expenseId,date,amt,description,"1");
         call.enqueue(new Callback<AddSTRExpenseResponse>() {
             @Override
             public void onResponse(Call<AddSTRExpenseResponse> call, Response<AddSTRExpenseResponse> response) {
                 if (response.code()==200)
                 {
                     dialog.dismiss();
                     addExpenseDialog.dismiss();
                     Toast.makeText(dashboard, "Expense Added", Toast.LENGTH_SHORT).show();
                     onResume();
                 }
                 else
                 {
                     dialog.dismiss();
                     Converter<ResponseBody, ApiError> converter =
                             ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                     ApiError error;
                     try {
                         error = converter.convert(response.errorBody());
                         ApiError.StatusBean status=error.getStatus();
                         Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                     } catch (IOException e) { e.printStackTrace(); }
                 }
             }

             @Override
             public void onFailure(Call<AddSTRExpenseResponse> call, Throwable t) {
                 Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
             }
         });
     }
   }

    private void getExpenseList()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
        Call<STRExpenseListResponse> call=apiInterface.getSTRExpenseList();
        call.enqueue(new Callback<STRExpenseListResponse>() {
            @Override
            public void onResponse(Call<STRExpenseListResponse> call, Response<STRExpenseListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    allexpenseList=response.body().getData();
                    recyclerView.setAdapter(new STRExpenseAdapter(dashboard,allexpenseList));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRExpenseListResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
