package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.rtrapp.R;

import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.STRModule.strAdapter.LatestSTRDailySheetAdapter;
import kashyap.chandan.rtrapp.STRModule.strAdapter.LatestSTRExpenseAdapter;
import kashyap.chandan.rtrapp.STRModule.strAdapter.LatestSTRInvestmentAdapter;
import kashyap.chandan.rtrapp.STRModule.strAdapter.LatestSTRWidhdrawalAdapter;
import kashyap.chandan.rtrapp.response.str.STRDailySheetListResponse;
import kashyap.chandan.rtrapp.response.str.STRDashboardResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseListResponse;
import kashyap.chandan.rtrapp.response.str.STRLatestInvestmentResponse;
import kashyap.chandan.rtrapp.response.str.STRWithdrawalListRsponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
public class STRHomeFragment extends Fragment implements View.OnClickListener {
    Context context;
    STRDashboard dashboard;
    RecyclerView recyclerView;
    Dialog dialog;
    TextView investmentcount,tv_profit_count,tv_withdrawal_count,tv_available_balance,tv_expense_count;
    RelativeLayout layout_daily,layout_expense,layout_withdrawal,layout_investment;
    List<STRDailySheetListResponse.DataBean>dailySheet=new ArrayList<>();
    List<STRExpenseListResponse.DataBean> allexpenseList=new ArrayList<>();
    List<STRWithdrawalListRsponse.DataBean>dataBeans=new ArrayList<>();
    List<STRLatestInvestmentResponse.DataBean>investmentLatest=new ArrayList<>();
    public STRHomeFragment(Context context) {
        this.dashboard= (STRDashboard) context;
        this.context=context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view= inflater.inflate(R.layout.home_fragment,container,false);
       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.recyclerView);
        layout_daily=view.findViewById(R.id.layout_daily);
        investmentcount=view.findViewById(R.id.investmentcount);
        tv_profit_count=view.findViewById(R.id.tv_profit_count);;
        tv_withdrawal_count=view.findViewById(R.id.tv_withdrawal_count);
        tv_available_balance=view.findViewById(R.id.tv_available_balance);
        tv_expense_count=view.findViewById(R.id.tv_expense_count);
        dialog=new Dialog(dashboard);
                layout_expense=view.findViewById(R.id.layout_expense);
                layout_withdrawal=view.findViewById(R.id.layout_withdrawal);
                layout_investment=view.findViewById(R.id.layout_investment);
                layout_investment.setOnClickListener(this);
                layout_withdrawal.setOnClickListener(this);
                layout_expense.setOnClickListener(this);
                layout_daily.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));
    }

    @Override
    public void onResume() {
        super.onResume();
        dashboard();
        layout_daily.setBackground(getContext().getDrawable(R.drawable.total_profit_background));
        layout_expense.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
        layout_investment.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
        layout_withdrawal.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
        getDailySheet();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.layout_daily:
                layout_daily.setBackground(getContext().getDrawable(R.drawable.total_profit_background));
                layout_expense.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_investment.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_withdrawal.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                getDailySheet();
                break;
            case R.id.layout_expense:
                layout_expense.setBackground(getContext().getDrawable(R.drawable.total_profit_background));
                layout_daily.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_investment.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_withdrawal.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                getExpenseList();
                break;
            case R.id.layout_investment:
                layout_investment.setBackground(getContext().getDrawable(R.drawable.total_profit_background));
                layout_daily.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_expense.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_withdrawal.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                getInvestment();
                break;
            case R.id.layout_withdrawal:
                layout_withdrawal.setBackground(getContext().getDrawable(R.drawable.total_profit_background));
                layout_daily.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_expense.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
                layout_investment.setBackground(getContext().getDrawable(R.drawable.total_expense_background));
               getWithrawal();
                break;
        }
    }
    private void dashboard()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRDashboardResponse>call=apiInterface.getSTRResponse();
        call.enqueue(new Callback<STRDashboardResponse>() {
            @Override
            public void onResponse(Call<STRDashboardResponse> call, Response<STRDashboardResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    tv_available_balance.setText(String.valueOf(response.body().getCurrentbalance()));
                    if (response.body().getTotalexpense().getSum()==null)
                        tv_expense_count.setText("0");
                    else  tv_expense_count.setText(response.body().getTotalexpense().getSum());
                    if (response.body().getTotalwithdrawal().getSum()==null)
                        tv_withdrawal_count.setText("0");
                    else   tv_withdrawal_count.setText(response.body().getTotalwithdrawal().getSum());
                    if (response.body().getTotalinvestment().getSum()==null)
                        investmentcount.setText("0");
                    else  investmentcount.setText(response.body().getTotalinvestment().getSum());
                    if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_profit_count.setText("0");
                    else if (response.body().getTotalProfit().getProfitsum()==null&&response.body().getTotalLoss().getLosssum()!=null)
                        tv_profit_count.setText("-"+response.body().getTotalLoss().getLosssum());
                    else if (response.body().getTotalProfit().getProfitsum()!=null&&response.body().getTotalLoss().getLosssum()==null)
                        tv_profit_count.setText("-"+response.body().getTotalProfit().getProfitsum());
                    else
                    {double profit_loss=Double.parseDouble(response.body().getTotalProfit().getProfitsum())-Double.parseDouble(response.body().getTotalLoss().getLosssum());
                        tv_profit_count.setText(String.valueOf(profit_loss));}
                }
                else
                {
                    dialog.dismiss();
                    tv_available_balance.setText("0");
                    tv_expense_count.setText("0");
                    tv_expense_count.setText("0");
                    investmentcount.setText("0");
                    tv_profit_count.setText("0");
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRDashboardResponse> call, Throwable t) {
dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getDailySheet()
    {
        recyclerView.setAdapter(null);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
        Call<STRDailySheetListResponse> call=apiInterface.getSTRDailySheet();
        call.enqueue(new Callback<STRDailySheetListResponse>() {
            @Override
            public void onResponse(Call<STRDailySheetListResponse> call, Response<STRDailySheetListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    dailySheet=response.body().getData();
                    recyclerView.setAdapter(new LatestSTRDailySheetAdapter(dashboard,dailySheet));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRDailySheetListResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getExpenseList()
    {
        recyclerView.setAdapter(null);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
        Call<STRExpenseListResponse> call=apiInterface.getSTRExpenseList();
        call.enqueue(new Callback<STRExpenseListResponse>() {
            @Override
            public void onResponse(Call<STRExpenseListResponse> call, Response<STRExpenseListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    allexpenseList=response.body().getData();
                    recyclerView.setAdapter(new LatestSTRExpenseAdapter(dashboard,allexpenseList));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRExpenseListResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void getWithrawal()
    {
        recyclerView.setAdapter(null);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRWithdrawalListRsponse> call=apiInterface.getSTRWithdrawals();
        call.enqueue(new Callback<STRWithdrawalListRsponse>() {
            @Override
            public void onResponse(Call<STRWithdrawalListRsponse> call, Response<STRWithdrawalListRsponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    dataBeans=response.body().getData();
                    recyclerView.setAdapter(new LatestSTRWidhdrawalAdapter(dashboard,dataBeans));

                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRWithdrawalListRsponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getInvestment()
    {
        recyclerView.setAdapter(null);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(false);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<STRLatestInvestmentResponse> call=apiInterface.getLatestSTRInvestment();
        call.enqueue(new Callback<STRLatestInvestmentResponse>() {
            @Override
            public void onResponse(Call<STRLatestInvestmentResponse> call, Response<STRLatestInvestmentResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    investmentLatest=response.body().getData();
                    recyclerView.setAdapter(new LatestSTRInvestmentAdapter(dashboard,investmentLatest));

                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRLatestInvestmentResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
