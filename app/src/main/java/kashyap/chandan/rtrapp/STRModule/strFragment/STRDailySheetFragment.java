package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.RTRModule.adapters.DailySheetAdapter;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.STRModule.strAdapter.STRDailySheetAdapter;
import kashyap.chandan.rtrapp.response.rtr.AddProfitLossResponse;
import kashyap.chandan.rtrapp.response.rtr.DailySheetListResponse;
import kashyap.chandan.rtrapp.response.str.AddSTRProfitLossResponse;
import kashyap.chandan.rtrapp.response.str.STRDailySheetListResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRDailySheetFragment extends Fragment {
    RecyclerView recyclerView;
    Context context;
    STRDashboard dashboard;
    Calendar calendar;
    DatePickerDialog.OnDateSetListener dateDialog;
    List<STRDailySheetListResponse.DataBean> dailySheet=new ArrayList<>();
    CircleImageView add;
    double initialprofit;
    double initialloss;
    double finalprofit;
    double finalloss;
    Dialog addprofit_loss_dialog,dialog;
    public STRDailySheetFragment(Context context) {
        this.context = context;
        this.dashboard = (STRDashboard) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view=inflater.inflate(R.layout.daily_sheet_fragment,container,false);
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView=view.findViewById(R.id.recyclerView);
        calendar=Calendar.getInstance();
        add=view.findViewById(R.id.add);
        add.setVisibility(View.GONE);
        dialog=new Dialog(dashboard);
        recyclerView.setLayoutManager(new LinearLayoutManager(dashboard,LinearLayoutManager.VERTICAL,false));


    }

    @Override
    public void onResume() {
        super.onResume();
        getDailySheet();
    }


    private String  updateLabel(Calendar calendar1) {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        System.out.println(sdf.format(calendar1.getTime()));
        return sdf.format(calendar1.getTime());
    }


    private void getDailySheet()
    {
        recyclerView.setAdapter(null);
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface=ApiClient.getClient().create(APIInterface.class);
        Call<STRDailySheetListResponse> call=apiInterface.getSTRDailySheet();
        call.enqueue(new Callback<STRDailySheetListResponse>() {
            @Override
            public void onResponse(Call<STRDailySheetListResponse> call, Response<STRDailySheetListResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    dailySheet=response.body().getData();
                    recyclerView.setAdapter(new STRDailySheetAdapter(dashboard,dailySheet));
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<STRDailySheetListResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
