package kashyap.chandan.rtrapp.STRModule.strAdapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.WithdrawalListRsponse;
import kashyap.chandan.rtrapp.response.str.STRWithdrawalListRsponse;

public class LatestSTRWidhdrawalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    private static final int TYPE_HEADER=0;
    private static final int TYPE_ITEM=1;
    List<STRWithdrawalListRsponse.DataBean> dataBeans;
    public LatestSTRWidhdrawalAdapter(Context context, List<STRWithdrawalListRsponse.DataBean> dataBeans) {
        this.context = context;
        this.dataBeans=dataBeans;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==TYPE_ITEM)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.withdraw_layout,parent,false);
            return new ItemViewHolder(view);
        }
       else if (viewType==TYPE_HEADER)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.withdraw_header,parent,false);
            return new HeaderViewHolder(view);
        }
       else
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
if (holder instanceof ItemViewHolder)
{
    final ItemViewHolder viewHolder= (ItemViewHolder) holder;
    viewHolder.tv_amount.setText(dataBeans.get(position-1).getAmount());
    viewHolder.tv_requested_date.setText(dataBeans.get(position-1).getRequestedDate());
    viewHolder.tv_reflected_date.setText(dataBeans.get(position-1).getReflectedDate());

}
        else if (holder instanceof HeaderViewHolder)
        {
            final HeaderViewHolder viewHolder= (HeaderViewHolder) holder;
        }
    }

    @Override
    public int getItemCount() {
        if (dataBeans.size()<10)
        return dataBeans.size()+1;
        else
            return 11;
        //Size should be +1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
        {
            return TYPE_HEADER;
        }
        return TYPE_ITEM ;
    }
    private class HeaderViewHolder  extends RecyclerView.ViewHolder
    {

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    private class ItemViewHolder  extends RecyclerView.ViewHolder
    {
TextView tv_requested_date,tv_amount,tv_reflected_date;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_amount=itemView.findViewById(R.id.tv_amount);
            tv_reflected_date=itemView.findViewById(R.id.tv_reflected_date);
            tv_requested_date=itemView.findViewById(R.id.tv_requested_date);
        }
    }
}
