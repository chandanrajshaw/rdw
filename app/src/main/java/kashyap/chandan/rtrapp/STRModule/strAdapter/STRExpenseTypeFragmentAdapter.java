package kashyap.chandan.rtrapp.STRModule.strAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.response.rtr.ExpenseTypeResponse;
import kashyap.chandan.rtrapp.response.str.STRExpenseTypeResponse;

public class STRExpenseTypeFragmentAdapter extends RecyclerView.Adapter<STRExpenseTypeFragmentAdapter.MyViewHolder> {
    Context context;
    List<STRExpenseTypeResponse.DataBean>expenseList;
    CustomItemClickListener listener;
    public STRExpenseTypeFragmentAdapter(Context context,  List<STRExpenseTypeResponse.DataBean>expenseList, CustomItemClickListener listener) {
   this.context=context;
   this.listener=listener;
   this.expenseList=expenseList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.expense_type_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.tv_expense_type.setText(expenseList.get(position).getStrexpensetype());
    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_expense_type;
        ImageView iv_edit;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_edit=itemView.findViewById(R.id.iv_edit);
            tv_expense_type=itemView.findViewById(R.id.tv_expense_type);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(view,expenseList.get(getAdapterPosition()).getId(),expenseList.get(getAdapterPosition()).getStrexpensetype());
                }
            };
            iv_edit.setOnClickListener(clickListener);
        }
    }
}
