package kashyap.chandan.rtrapp.STRModule.strAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.rtrapp.CustomItemClickListener;
import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.STRModule.ViewSTRInvestment;
import kashyap.chandan.rtrapp.ViewInvestment;
import kashyap.chandan.rtrapp.response.rtr.InvestmentListResponse;
import kashyap.chandan.rtrapp.response.str.STRInvestmentListResponse;

public class STRInvestAdapter extends RecyclerView.Adapter<STRInvestAdapter.MyViewHolder> {
    Context context;
    CustomItemClickListener listener;
    List<STRInvestmentListResponse.DataBean> dataBeans;
    public STRInvestAdapter(Context context, List<STRInvestmentListResponse.DataBean> dataBeans, CustomItemClickListener listener) {
        this.context=context;
        this.listener=listener;
        this.dataBeans=dataBeans;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.investment_list_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_date.setText(dataBeans.get(position).getInvestDate());
        holder.tv_investment_amount.setText(dataBeans.get(position).getAmount());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ViewSTRInvestment.class);
                intent.putExtra("date",dataBeans.get(position).getInvestDate());
                intent.putExtra("amount",dataBeans.get(position).getAmount());
                intent.putExtra("id",dataBeans.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_edit;
        TextView tv_investment_amount,tv_date;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_investment_amount=itemView.findViewById(R.id.tv_investment_amount);
            tv_date=itemView.findViewById(R.id.tv_date);
            iv_edit=itemView.findViewById(R.id.iv_edit);
            View.OnClickListener clickListener=new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(view,dataBeans.get(getAdapterPosition()).getId(),dataBeans.get(getAdapterPosition()).getAmount());
                }
            };
            iv_edit.setOnClickListener(clickListener);
        }
    }
}
