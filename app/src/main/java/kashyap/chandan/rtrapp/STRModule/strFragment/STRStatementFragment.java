package kashyap.chandan.rtrapp.STRModule.strFragment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.rtrapp.R;
import kashyap.chandan.rtrapp.RTRModule.MainActivity;
import kashyap.chandan.rtrapp.STRModule.STRDashboard;
import kashyap.chandan.rtrapp.response.rtr.RTRStatementResponse;
import kashyap.chandan.rtrapp.retrofit.APIInterface;
import kashyap.chandan.rtrapp.retrofit.ApiClient;
import kashyap.chandan.rtrapp.retrofit.ApiError;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class STRStatementFragment extends Fragment {
    STRDashboard dashboard;
    TableLayout tl ;
    Dialog dialog;
    public STRStatementFragment(STRDashboard dashboard) {
        this.dashboard = dashboard;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.statement_fragment,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         tl = view.findViewById(R.id.table);
         dialog=new Dialog(dashboard);
         addData();
//        getStatement();
    }

    private TextView getTextView (int id, String title, int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(dashboard);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
    public void addData() {

        // int numCompanies = companies.length;
        TableRow tr = new TableRow(dashboard);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Date", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Investment", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, " Expense", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Withdrawal", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Profit/Loss", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tr.addView(getTextView(0, "Available", ContextCompat.getColor(dashboard, R.color.txt_color), Typeface.BOLD, ContextCompat.getColor(dashboard, R.color.white)));
        tl.addView(tr, getTblLayoutParams());
////        tl.removeAllViews();
//        double balance=0;
//        int size=dataBean.size();
//        for (int i = 0; i < size; i++) {
//            TableRow trr = new TableRow(dashboard);
//            trr.setLayoutParams(getLayoutParams());
//            TransactionResponse.DataBean transaction=dataBean.get(i);

//            trr.addView(getTextView( 1,""+(i+1), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
//            trr.addView(getTextView(1, transaction.getT_date(),ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
//            trr.addView(getTextView(1,transaction.getPaid_amount(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
//            trr.addView(getTextView(1, transaction.getLend_amount(), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL, ContextCompat.getColor(this, R.color.textcolor)));
//            double paid=Double.parseDouble(transaction.getPaid_amount());
//            double lend=Double.parseDouble(transaction.getLend_amount());
//            balance=balance+lend;
//            balance=balance-paid;
//            trr.addView(getTextView(1, String.valueOf(balance), ContextCompat.getColor(this, R.color.background_color), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.textcolor)));
//            tl.addView(trr, getTblLayoutParams());

    }


    private  void getStatement()
    {
        dialog.setContentView(R.layout.loadingdialog);
        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        APIInterface apiInterface= ApiClient.getClient().create(APIInterface.class);
        Call<RTRStatementResponse> call=apiInterface.getstatement();
        call.enqueue(new Callback<RTRStatementResponse>() {
            @Override
            public void onResponse(Call<RTRStatementResponse> call, Response<RTRStatementResponse> response) {
                if (response.code()==200)
                {
                    dialog.dismiss();
                    String url=response.body().getExcelDailysheet();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                else
                {
                    dialog.dismiss();
                    Converter<ResponseBody, ApiError> converter =
                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                    ApiError error;
                    try {
                        error = converter.convert(response.errorBody());
                        ApiError.StatusBean status=error.getStatus();
                        Toast.makeText(dashboard, ""+status.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) { e.printStackTrace(); }
                }
            }

            @Override
            public void onFailure(Call<RTRStatementResponse> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(dashboard, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
